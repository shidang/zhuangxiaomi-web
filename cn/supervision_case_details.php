<?php
    session_start();
    include("../php/include.php");
    $aid = $_GET["aid"];
    $sql = mysql_query("SELECT * FROM supervision_case WHERE id=$aid");
    $row = mysql_fetch_assoc($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>监理案例详情页</title>
    <meta name="renderer" content="webkit">
    <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
    <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <!-- <link rel="stylesheet" type="text/css" href="../css/login_register.css" /> -->
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
</head>
<body>
    <div class="wrap">
        <?php
            include("head.php");
        ?>
    </div>
    <div class="details clearfix">
        <div class="nav">
            <a href="index.php">首页</a>
            <i>></i>
            <a href="supervision_case.php">监理案例</a>
            <i>></i>
            <a href="supervision_case_details.php">北京麒麟瑞景别墅</a>
        </div>
        <!-- 图文介绍模块 -->
        <div class="basic-main clearfix">
            <div class="b-left clearfix">
                <div class="b-left-img clearfix">
                    <div class="pic-show">
                        <img src="../images/demo_images/12-1.jpg" alt="SSD" class="big-pic" />
                        <img src="../images/demo_images/12-2.jpg" alt="SSD" />
                        <img src="../images/demo_images/12-3.jpg" alt="DDS" />
                    </div>
                    <div class="pic-more">
                        <img src="../images/demo_images/12-4.jpg" alt="SSD" />
                        <div class="num">
                            <span class="num-logo"></span>
                            <em class="pic-count">1</em>
                        </div>
                    </div>
                </div>
                <dl class="info">
                    <dt class="info-tit">
                        <em><?php echo $row["houseName"]; ?></em>
                        <span>已完工</span>
                    </dt>
                    <dd class="info-con">
                        <span>面积：</span>
                        <em>135m<sup>2</sup>
                        </em>
                    </dd>
                    <dd class="info-con">
                        <span>决算：</span>
                        <em>23万元（预算：36万元）</em>
                    </dd>
                    <dd class="info-con">
                        <span>实际工期：</span>
                        <em>32天（预计工期：45天）</em>
                    </dd>
                    <dd class="info-con">
                        <span>业主：</span>
                        <em>李女士</em>
                    </dd>
                    <dd class="info-con">
                        <span>工长：</span>
                        <em>张元培</em>
                    </dd>
                    <dd class="info-con">
                        <span>工程评价：</span>
                        <em class="score">8.6分</em>
                        <a href="">详情 ></a>
                    </dd>
                </dl>
            </div>
            <div class="b-right">
                <dl class="peo-info">
                    <dt>
                        <?php echo $row["engineer_pic"]; ?>
                    </dt>
                    <dd class="person">
                        <span><?php echo $row["engineer"]; ?></span>
                        <em>监理</em>
                    </dd>
                    <dd>
                        <span>监理级别：</span>
                        <em class="black-star"></em>
                    </dd>
                    <dd>
                        <span>持证：</span>
                        <em>监理工程师</em>
                    </dd>
                    <dd>
                        <span>从业：</span>
                        <em>10年以上</em>
                    </dd>
                </dl>
            </div>
        </div>
        <!-- 查看大图模块 -->
        <div class="big-img-sd">
            <div class="con-img">
                <div class="con-dicm">
                     <img src="../images/demo_images/12-1.jpg" alt="SSD" title="SSD">
                     <img src="../images/demo_images/12-2.jpg" alt="SSD" title="SSD">
                     <img src="../images/demo_images/12-3.jpg" alt="SSD" title="SSD">
                      <img src="../images/demo_images/12-4.jpg" alt="SSD" title="SSD">
                </div>      
            </div>
            <span class="close">X</span>
            <div class="page">
                <span class="this-page">1</span> /
                <span class="all-page">22</span>
             </div>
            <div class="img-info">
                <h3>体验细节验收标准透明</h3>
                <p>装小蜜提供只会不高于家装修公司的初步评测；体验项细节验收标准透明确保家装工程质量；准备装修不知道该如何选择适合自己装修业材料以次充好的情造成业主实际花费远超出预算；外径在25mm以管道在转表可能会导致设计的效果完全无法施预算不详细则</p>
            </div>
            <div class = "img-le"></div>
            <div class = "img-ri"></div> 
        </div>
        <!-- 项目分类模块 -->
        <div class="project" id="project-sd">
            <div class="pro-tit">
                <a href="#before-sd" class="first change">前期审核</a>
                <a href="#basic-sd" class="bg">工程交底</a>
                <a href="#hides-sd" class="bg">隐蔽工程</a>
                <a href="#medium-sd" class="bg">中期验收</a>
                <a href="#complete-sd" class="bg">竣工验收</a>
            </div>
            <div class="pro-oper">
                <a href="#accept-sd">变更项目</a>
                <a href="#accept-sd">材料验收</a>
            </div>
            <div class="pro-speed">
                <span>完成进度 ：<?php echo $row["step"]; ?></span>
                <em style="width: 240px"></em>
                <i style="width: <?php echo $row    ["step"]; ?> "></i>
            </div>
        </div>
        <!-- 预算审核模块 -->
        <div class="con-before clearfix" id="before-sd" name="before-sd">
            <div class="bef-tit">
                <h2>前期审核</h2>
                <span>审核2项，存在问题1项。</span>
            </div>
            <div class="bef-left clearfix">
                <h3 class="bef-head">
                    <span>预算审核 </span>
                    <a href="">【下载】</a>
                    <em>审核执行标准：装小蜜北京地区预算审核标准</em>
                </h3>
                <ul class="bef-detail clearfix">
                    <li>
                        <span>审核人</span>
                        <em>周树人</em>
                    </li>
                    <li>
                        <span>审核时间</span>
                        <em>2015.03.15</em>
                    </li>
                    <li>
                        <span>审核数量</span>
                        <em>共审核3份报价216项</em>
                    </li>
                    <li>
                        <span>审核文件</span>
                        <em class="bold">[ 审核文件下载 ]</em>
                    </li>
                </ul>
                <ul class="bef-list clearfix">
                    <li>
                        <span>单价属于合理范围</span>
                        <em class="check"></em>
                    </li>
                    <li>
                        <span>报价符合设计方案</span>
                        <em class="check"></em>
                    </li>
                    <li>
                        <span>材料与施工工艺符合</span>
                        <em class="check"></em>
                    </li>
                    <li>
                        <span>无重复报价</span>
                        <em class="check"></em>
                    </li>
                </ul>
               <ul class="bef-list clearfix">
                    <li>
                        <span>无重复工艺</span>
                        <em class="check"></em>
                    </li>
                    <li>
                        <span>无偷项漏项</span>
                        <em class="check"></em>
                    </li>
                    <li>
                        <span>无重复收费</span>
                        <em class="check"></em>
                    </li>
                    <li>
                        <span>-</span>
                        <em></em>
                    </li>
                </ul>
            </div>
            <div class="bef-right">
                <h3>注意事项</h3>
                <ul>
                    <li class="points">外径在25mm以管道在转表笼头处管道牢固</li>
                    <li class="points">墙体终端处设立管卡并安装不可用需改造。</li>
                    <li class="points">可能会导致设效果完全无法施预算不详细则。</li>
                    <li class="points">装小蜜提供只会不高于家装修公司步。</li>
                    <li class="points">体验项细节验收标准透明确保家装工程量。</li>
                </ul>
            </div>
            <div class="bef-left clearfix">
                <h3 class="bef-head clearfix">
                    <span>图纸审核 </span>
                    <a href="">【下载】</a>
                    <em>审核执行标准：房屋建筑室内装饰装修制图标准(JGJ/T 244-2011)</em>
                </h3>
                <ul class="bef-detail clearfix">
                    <li>
                        <span>审核人</span>
                        <em>周树人</em>
                    </li>
                    <li>
                        <span>审核时间</span>
                        <em>2015.03.15</em>
                    </li>
                    <li>
                        <span>审核数量</span>
                        <em>共审核5张图纸</em>
                    </li>
                    <li>
                        <span>-</span>
                        <em class="bold"></em>
                    </li>
                </ul>
                <ul class="bef-list clearfix">
                    <li>
                        <span>图纸全面</span>
                        <em class="imglogo"></em>
                    </li>
                    <li>
                        <span>符合图纸规范</span>
                        <em class="imglogo"></em>
                    </li>
                    <li>
                        <span>拆改无违规事项</span>
                        <em class="imglogo"></em>
                    </li>
                    <li>
                        <span>材料使用符合规范</span>
                        <em class="imglogo"></em>
                    </li>
                </ul>
               <ul class="bef-list clearfix">
                    <li>
                        <span>无重复工艺</span>
                        <em class="imglogo"></em>
                    </li>
                    <li>
                        <span>无偷项漏项</span>
                        <em class="imglogo"></em>
                    </li>
                    <li>
                        <span>无重复收费</span>
                        <em class="imglogo"></em>
                    </li>
                    <li>
                        <span>-</span>
                        <em></em>
                    </li>
                </ul>
            </div>
            <div class="bef-right">
                <h3>注意事项</h3>
                <ul>
                    <li class="points">外径在25mm以管道在转表笼头处管道牢固</li>
                    <li class="points">墙体终端处设立管卡并安装不可用需改造。</li>
                    <li class="points">可能会导致设效果完全无法施预算不详细则。</li>
                    <li class="points">装小蜜提供只会不高于家装修公司步。</li>
                    <li class="points">体验项细节验收标准透明确保家装工程量。</li>
                </ul>
            </div>
        </div>
        <!-- 工程交底模块 -->
        <div class="con-basic clearfix" id="basic-sd" name="basic-sd">
            <div class="ba-tit">
                <h2>工程交底</h2>
                <span>审核13项，存在问题2项。</span>
                <div class="ba-point">
                    <em class="null"></em>
                    <span>无内容</span>
                    <em class="check"></em>
                    <span>合格项</span>
                    <em class="imglogo"></em>
                    <span>图片项</span>
                    <em class="trouble"></em>
                    <span>问题项</span>  
                </div>
            </div>
            <div class="ba-info">
                <div class="list clearfix">
                   <ul class="bef-list clearfix">
                        <li>
                            <span>图纸全面</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li>
                            <span>图纸全面</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li>
                            <span>图纸全面</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="num-change">2</em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li>
                            <span>图纸全面</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                    </ul>
                </div>
                <table class="problem clearfix">
                    <thead>
                        <tr>
                            <th>编号</th>
                            <th>问题项</th>
                            <th>问题原因</th>
                            <th>发现日期</th>
                            <th>解决日期</th>
                            <th>问题结果</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">1</div>
                            </td>
                            <td class="pro-point"> 弱点导线存在问题  </td>
                            <td class="pro-reason"> 弱点导线风口出现异常需调整（图）</td>
                            <td class="date">2015.03.21 </td>
                            <td class="date">2015.03.23 </td>
                            <td class="result">问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">2</div>
                            </td>
                            <td>  接线分色存在隐患问题  </td>
                            <td>接线分色出现异常需要调整位置（图）</td>
                            <td>2015.03.26  </td>
                            <td> 2015.03.28 </td>
                            <td>问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- 隐蔽工程模块 -->
        <div class="con-basic clearfix" id="hides-sd" name="hides-sd">
            <div class="ba-tit">
                <h2>隐蔽工程</h2>
                <span>审核13项，存在问题2项。</span>
                <div class="ba-point">
                    <em class="null"></em>
                    <span>无内容</span>
                    <em class="check"></em>
                    <span>合格项</span>
                    <em class="imglogo"></em>
                    <span>图片项</span>
                    <em class="trouble"></em>
                    <span>问题项</span>  
                </div>
            </div>
            <div class="ba-info">
                <div class="list clearfix">
                   <ul class="bef-list  clearfix">
                        <li class="list-tit">
                            电路改造
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list  clearfix">
                        <li class="list-tit">
                            水路改造
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li class="list-tit">
                            隐蔽工程验收
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="num-change">2</em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                         <li class="list-tit">
                            排除及新建项目
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                         <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                    </ul>
                </div>
                <table class="problem clearfix">
                    <thead>
                        <tr>
                            <th>编号</th>
                            <th>问题项</th>
                            <th>问题原因</th>
                            <th>发现日期</th>
                            <th>解决日期</th>
                            <th>问题结果</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">1</div>
                            </td>
                            <td class="pro-point"> 弱点导线存在问题  </td>
                            <td class="pro-reason"> 弱点导线风口出现异常需调整（图）</td>
                            <td class="date">2015.03.21 </td>
                            <td class="date">2015.03.23 </td>
                            <td class="result">问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">2</div>
                            </td>
                            <td>  接线分色存在隐患问题  </td>
                            <td>接线分色出现异常需要调整位置（图）</td>
                            <td>2015.03.26  </td>
                            <td> 2015.03.28 </td>
                            <td>问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- 中期验收模块 -->
        <div class="con-basic clearfix" id="medium-sd" name="medium-sd">
            <div class="ba-tit">
                <h2>中期验收</h2>
                <span>审核13项，存在问题2项。</span>
                <div class="ba-point">
                    <em class="null"></em>
                    <span>无内容</span>
                    <em class="check"></em>
                    <span>合格项</span>
                    <em class="imglogo"></em>
                    <span>图片项</span>
                    <em class="trouble"></em>
                    <span>问题项</span>  
                </div>
            </div>
            <div class="ba-info">
                <div class="list clearfix">
                   <ul class="bef-list  clearfix">
                        <li class="list-tit">
                            饰面砖
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list  clearfix">
                        <li class="list-tit">
                            油饰
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                         <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li class="list-tit">
                            细木、吊顶
                        </li>
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="num-change">2</em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                         <li class="list-tit">
                            验收
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                         <li>
                            <span>符合图纸规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                         <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                    </ul>
                </div>
                <table class="problem clearfix">
                    <thead>
                        <tr>
                            <th>编号</th>
                            <th>问题项</th>
                            <th>问题原因</th>
                            <th>发现日期</th>
                            <th>解决日期</th>
                            <th>问题结果</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">1</div>
                            </td>
                            <td class="pro-point"> 弱点导线存在问题  </td>
                            <td class="pro-reason"> 弱点导线风口出现异常需调整（图）</td>
                            <td class="date">2015.03.21 </td>
                            <td class="date">2015.03.23 </td>
                            <td class="result">问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">2</div>
                            </td>
                            <td>  接线分色存在隐患问题  </td>
                            <td>接线分色出现异常需要调整位置（图）</td>
                            <td>2015.03.26  </td>
                            <td> 2015.03.28 </td>
                            <td>问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- 竣工验收模块 -->
        <div class="con-basic clearfix" id="complete-sd" name="complete-sd">
            <div class="ba-tit">
                <h2>竣工验收</h2>
                <span>审核13项，存在问题2项。</span>
                <div class="ba-point">
                    <em class="null"></em>
                    <span>无内容</span>
                    <em class="check"></em>
                    <span>合格项</span>
                    <em class="imglogo"></em>
                    <span>图片项</span>
                    <em class="trouble"></em>
                    <span>问题项</span>  
                </div>
            </div>
            <div class="ba-info">
                <div class="list clearfix">
                   <ul class="bef-list clearfix">
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li>
                            <span>符合图纸规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>拆改无违规事项</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="num-change">1</em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                    </ul>
                     <ul class="bef-list clearfix">
                        <li>
                            <span>材料使用符合规范</span>
                            <em class="imglogo"></em>
                        </li>
                         <li>
                            <span>材料使用符合规范</span>
                            <em class="check"></em>
                        </li>
                        <li>
                            <span>-</span>
                            <em></em>
                        </li>
                    </ul>
                </div>
                <table class="problem clearfix">
                    <thead>
                        <tr>
                            <th>编号</th>
                            <th>问题项</th>
                            <th>问题原因</th>
                            <th>发现日期</th>
                            <th>解决日期</th>
                            <th>问题结果</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">1</div>
                            </td>
                            <td class="pro-point"> 弱点导线存在问题  </td>
                            <td class="pro-reason"> 弱点导线风口出现异常需调整（图）</td>
                            <td class="date">2015.03.21 </td>
                            <td class="date">2015.03.23 </td>
                            <td class="result">问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                        <tr>
                            <td class="table-num">
                                <div class="num-index">2</div>
                            </td>
                            <td>  接线分色存在隐患问题  </td>
                            <td>接线分色出现异常需要调整位置（图）</td>
                            <td>2015.03.26  </td>
                            <td> 2015.03.28 </td>
                            <td>问题已解决，弱点导线通畅。（图）</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- 材料验收及验收项目 -->
        <div class="con-basic clearfix" id="accept-sd" name="accept-sd">
            <div class="ba-tit">
                <h2>材料验收及验收项目</h2>
                <span>材料验收13项，变更项目6项。</span>   
            </div>
            <table class="accept-change clearfix">
                <caption>材料验收</caption>
                <thead>
                    <tr>
                        <th>材料名称</th>
                        <th>品牌名称</th>
                        <th>验收结果</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                    </tr>
                    <tr>
                        <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                    </tr>
                    <tr>
                        <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                    </tr>
                </tbody>
            </table>
             <table class="accept-change clearfix">
                <caption>变更项目</caption>
                <thead>
                    <tr>
                        <th>变更项目</th>
                        <th>变更工期</th>
                        <th>变更内容</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                    </tr>
                    <tr>
                        <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                    </tr>
                    <tr>
                        <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                       <td class="pro-point"> 弱点导线存在问题  </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- 工程评价模块 -->
        <div class="con-basic clearfix">
            <div class="ba-tit">
                <h2>工程评价</h2>
                <span>监理，业主，工长对工程的具体评价。</span>   
            </div>
            <dl class="assess">
                <dt>
                    <img src="../images/demo_images/peo.png" alt="" />
                </dt>
                <dd class="person">
                    <span>林云朗</span>
                    <em>监理</em>
                </dd>
                <dd>
                    <span>工程评分：</span>
                    <i class="white-star"></i>
                    <em>8.6分</em>
                </dd>
                <dd class="ass-con">
                   <p>传统的监理管家式监理不同于，传统监理只负责工程质量的验收，装小蜜更关注装修的整体除监协助业主筛选施工方程内质量。</p>
                </dd>
            </dl>
             <dl class="assess-owner">
                <dt>
                    <img src="../images/demo_images/peo.png" alt="" />
                </dt>
                <dd class="person">
                    <span>林云朗</span>
                    <em>监理</em>
                </dd>
                <dd>
                    <span>工程评分：</span>
                    <i class="white-star"></i>
                    <em>8.6分</em>
                </dd>
                <dd class="ass-con">
                   <p>传统的监理管家式监理不同于，传统监理只负责工程质量的验收，装小蜜更关注装修的整体除监协助业主筛选施工方程内质量。</p>
                </dd>
            </dl>
            <dl class="assess">
                <dt>
                    <img src="../images/demo_images/peo.png" alt="" />
                </dt>
                <dd class="person">
                    <span>林云朗</span>
                    <em>监理</em>
                </dd>
                <dd>
                    <span>工程评分：</span>
                    <i class="white-star"></i>
                    <em>8.6分</em>
                </dd>
                <dd class="ass-con">
                   <p>传统的监理管家式监理不同于，传统监理只负责工程质量的验收，装小蜜更关注装修的整体除监协助业主筛选施工方程内质量。</p>
                </dd>
            </dl>
        </div>
        <!-- 评论模块 -->
           <!--高速版-->
        <div id="SOHUCS"></div>
    </div>
    <!-- 图片滚动模块 -->
    <!-- 不包含主体中 -->
    <div class="scroll-sd">
        <div class="scoll-lr">
            <div class="pic-con" id = "main">
                <div class="pic clearfix" id="con">
                   <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>                     
                     <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                    <div class="posi">
                         <a href="supervision_case_details.php">
                            <img src="../images/demo_images/12-3.jpg" alt="##" class="dicm">
                        </a>
                        <dl class="per-show">
                            <dt>
                            <img src="../images/demo_images/peo.png" alt="##" /></dt>
                            <dd>
                                <h3>林一凡</h3>
                                <span>监理</span>
                            </dd>
                            <dd>
                                <p>北京紫禁城皇宫</p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class = "left" id = "left"></div>
        <div class = "right" id = "right"></div> 
    </div>
    <?php
        include("foot.html");
    ?>     
    <script type="text/javascript" src="../js/common.js"></script>
    <script charset="utf-8" type="text/javascript" src="http://changyan.sohu.com/upload/changyan.js" ></script>
    <script type="text/javascript">
        window.changyan.api.config({
            appid: 'cys38cm5U',
            conf: 'prod_c870d38ab7d12241bd4be213c98cea53'
        }); 

        /* 
        * @Author: ssd
        * @Date:   2015-10-18 11:23:45
        * @Last Modified by:   anchen
        * @Last Modified time: 2015-10-24 10:33:25
        * 监理案例详情页
        */

        // 定义本页对象命名
          var sd = {
            // 定义页面中各模块的高度的变量
            _top: 0,
            _sh: 0,
            be: 0,
            ba: 0,
            hi: 0,
            me: 0,
            co: 0,
            ac: 0,
            len: 0, // 定义图片长度变量
            leng: 0, //大图长度变量
            index: 1 // 大图计数变量
          };

         // 图文模块hover效果
         $('.details .b-left-img').hover(function(){
                $(this).find('.num').stop().animate({
                    bottom:'0px'
                },300);
            },function(){
                $(this).find('.num').stop().animate({
                    bottom:'-100px'
            },300);
                $('.details .pic-count').text(sd.leng);
        });

        // 获取页面各模块高度
        sd._top = $('#project-sd').offset().top;
        sd.be = $('#before-sd').offset().top;
        sd.ba = $('#basic-sd').offset().top;
        sd.hi = $('#hides-sd').offset().top;
        sd.me = $('#medium-sd').offset().top;
        sd.co = $('#complete-sd').offset().top;
        sd.ac = $('#accept-sd').offset().top;
        $(window).scroll(function(){
            sd._sh = $(this).scrollTop();
            // 标题定位
            if (sd._sh >= sd._top) {
                $('#project-sd').addClass('p-fixed');
                $('.details .basic-main').addClass('.details b-margin');
            }else{
                $('#project-sd').removeClass('p-fixed');
                $('.details .basic-main').removeClass('.details b-margin');
            }
            // 匹配高度换背景
            if (sd._sh <= sd.be) {
                $('.details .pro-tit a').eq(0).addClass('.details change').siblings().removeClass('change');
            }else if(sd._sh > sd.be && sd._sh<= sd.ba){
                $('.details .pro-tit a').eq(1).addClass('.details change').siblings().removeClass('change');
            }else if(sd._sh > sd.ba && sd._sh <= sd.hi){
                $('.details .pro-tit a').eq(2).addClass('.details change').siblings().removeClass('change');
            }else if(sd._sh > sd.hi && sd._sh <= sd.me){
                $('.details .pro-tit a').eq(3).addClass('.details change').siblings().removeClass('change');
            }else if(sd._sh > sd.me && sd._sh <= sd.co){
                $('.details .pro-tit a').eq(4).addClass('.details change').siblings().removeClass('change');
            }else if(sd._sh > sd.co){
                $('.details .pro-tit a').removeClass('.details change');
            }
        })
        // 滑动至a标签目标处
        $(".details .pro-tit a, .details .pro-oper a").click(function(event) {
            $("html, body").stop().animate({
                scrollTop: $($(this).attr("href")).offset().top - 165 + "px",
                easing: "swing"
            },200);
        })

        // 图片滚动模块
        sd.len = $('.scroll-sd #con a').length;
        $('.scroll-sd #con a').last().css({'margin': 0});
        $('.scroll-sd #con').width((sd.len * 280)-30);
        $('.scroll-sd #right').click(function(){
            $('.scroll-sd #main').stop().animate({
                scrollLeft: $('.scroll-sd #main').scrollLeft() + 280
            },600)  
        });
        $('.scroll-sd #left').click(function(){
            $('.scroll-sd #main').stop().animate({
                scrollLeft: $('.scroll-sd #main').scrollLeft() - 280
            },600)  
        });

        // 鼠标放上图片放大
        $('.scroll-sd .pic a').hover(function(){
                $(this).find('.dicm').stop().animate({
                    top: '-10px',
                    left: '-10px', 
                    width: '280px',
                    height: '220px'
                },300);
            },function(){
                $(this).find('.dicm').stop().animate({
                    top: '0px',
                    left: '0px',  
                    width: '245px',
                    height: '190px'
            },300);
        });


        // 查看大图功能
        $(".details .b-left-img, .imglogo").click(function() {
            $(".details .big-img-sd").stop().animate({
                "top" : "0",
                "bottom" : "0"
            }, 800, function() {
                $(".big-img-sd .page").fadeIn(500);
                $(".big-img-sd .img-le").fadeIn(500);
                $(".big-img-sd .img-ri").fadeIn(500);
                $(".big-img-sd .img-info").fadeIn(500);
                $(".big-img-sd .con-img").fadeIn(500);
                $(".big-img-sd span").fadeIn(500);
            });
             $('.big-img-sd .page .all-page').text(sd.leng);
             $('.big-img-sd .page .this-page').text(1);
        });
        $(".big-img-sd .close").click(function() {
            $(".big-img-sd .page").fadeOut(10);
            $(".big-img-sd .img-le").fadeOut(10);
            $(".big-img-sd .img-ri").fadeOut(10);
            $(".big-img-sd .img-info").fadeOut(10);
            $(".big-img-sd .con-img").fadeOut(1);
            $(".big-img-sd span").fadeOut(1);
            $(".big-img-sd").stop().animate({
                "top" : "50%",
                "bottom" : "50%"
            }, 800);
        })
        // 查看大图切换与计数功能 
        sd.index = 1;
        sd.leng = $('.big-img-sd .con-img img').length;
        $('.big-img-sd .con-dicm').width((sd.leng * 600));
        $('.big-img-sd .img-ri').click(function(){
            sd.index ++;
            if(sd.index >= sd.leng){
                sd.index = sd.leng;
            }
            $('.big-img-sd .con-img').stop().animate({
                scrollLeft: $('.big-img-sd .con-img').scrollLeft() + 600
            },600);
            $('.big-img-sd .page .all-page').text(sd.leng);
            $('.big-img-sd .page .this-page').text(sd.index);  
        });
        $('.big-img-sd .img-le').click(function(){
            sd.index --;
            if(sd.index <= 1){
                sd.index = 1;
            }
            $('.big-img-sd .con-img').stop().animate({
                scrollLeft: $('.big-img-sd .con-img').scrollLeft() - 600
            },600);
            $('.big-img-sd .page .all-page').text(sd.leng);
            $('.big-img-sd .page .this-page').text(sd.index);  
        });
    </script>   
</body>
</html>