<?php 
    session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>装小蜜家装监理-互联网家装监理服务</title>
		<link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
		<link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/index_layout.css" />
		<link rel="stylesheet" type="text/css" href="../css/login_register.css" />
	</head>
	<body>
		<?php
        	include("head.php");
    	?>
		<div class="content-cql">
			<div class="wrapbanner-cql">
				<ul class="banner-cql">
					<li class="banner-order">
						<img src="../images/demo_images/banner.jpg" alt="广告大图" title="">
						<!-- <div class="shade"></div> -->
						<div class="banner-til">
							<h2>家装监理装小蜜  省钱省心更省力</h2>
							<em>北京活动限时尊享 别墅及工装除外 4月15日前预定均有Apple Watch相送</em>
							<span>￥1999</span>
							<del class="orderinline-ql">马上预订</del>
						</div>
					</li>
					<li class="banner-team">
						<a href="supervision_team.php" title="">
							<img src="../images/demo_images/banner-team.jpg" alt="广告大图" title="监理侠联盟">
						</a>
					</li>
					<li class="banner-club">
						<a href="panner.php" title="">
							<img src="../images/demo_images/banner-club.jpg" alt="广告大图" title="蜜糖微信">
						</a>
					</li>
				</ul>
				<div class="footer-btn-cql">
					<span class="here-cql"></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="serintroduce">
				<div class="service-cql clearfix">
					<div class="supervision">
						<div class="title-cql clearfix">
							<em>SUPERVISION/</em>
							<span>监理服务介绍</span>
						</div>
						<p>装小蜜开创互联网家装监理服务理念，用专业角度帮助业主处理装修中的相关事宜，对装修工程进行全方位的监理，通过装小蜜积累的项目管理经验和行业资源，让您省钱、省心，省力的实现高质量的装修。</p>
						<p>互联网家装监理不同于传统的监理服务，传统监理只负责工程质量的验收，装小蜜更关注装修的整体方案，除了监理工程质量之外，还协助您筛选施工方，审核图纸预算，跟踪工程进度等，站在您的角度，从源头把控，全方位维护您利益。</p>
						<a href="supervision_service.php" title="家装监理介绍" class="more-cql">了解更多</a>
					</div>
					<div class="supervisionright">
						<img src="../images/demo_images/introduce.png" alt="图" title="监理服务介绍">
					</div>
				</div>
			</div>
			<div class="datasupport-cql">
				<div class="support-title">
					<h3>监理数据支持</h3>
					<p>135份专业报告，专业监理师全程一对一解答，帮你远离装修猫腻，把好装修质量关！</p>
				</div>
				<ul class="supportlist-cql">
					<li class="sup-supervision">
						<img src="../images/demo_images/datasupport.png" alt="图" title="">
						<div class="slider-cql">
							<div class="sliderhide">
								<h4>家装监理</h4>
								<span>4大项验收阶段 78项验收标准全程把关</span>
								<a href="supervision_service.php" title="家装监理介绍" target="_blank">了解更多</a>
							</div>
						</div>
					</li>
					<li class="sup-supervision">
						<img src="../images/demo_images/datasupport.png" alt="图" title="">
						<div class="slider-cql slider-log">
							<div class="sliderhide slider-log">
								<h4>监理日志</h4>
								<span>微信在线直播您的工地进度</span>
								<a href="supervision_case.php" title="家装监理工地进度" target="_blank">了解更多</a>
							</div>
						</div>
					</li>
					<li class="sup-supervision">
						<img src="../images/demo_images/datasupport.png" alt="图" title="">
						<div class="slider-cql">
							<div class="sliderhide slider-report">
								<h4>服务报告</h4>
								<span>装小蜜每日客户服务报告</span>
								<a href="article_list.php" title="家装监理服务报告" target="_blank">了解更多</a>
							</div>
						</div>
					</li>
					<li class="sup-supervision">
						<img src="../images/demo_images/datasupport.png" alt="图" title="">
						<div class="slider-cql">
							<div class="sliderhide slider-team">
								<h4>监理团队</h4>
								<span>国内顶级家装监理师团队 执行国家标准</span>
								<a href="supervision_team.php" title="监理团队" target="_blank">了解更多</a>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="datacase-cql">
				<div class="datacase-title">
					<em>监理案例展示</em>
					<p>120,000份监理案例，专业监理师全程监理，更详实的数据，省钱省心更省力！</p>
				</div>
				<div class="accordion">
					<div class="accordion-btn">
						<a href="supervision_case.php" target="_blank" title="" class="casemore-cql">更多案例</a>
					</div>
					<div class="accordion-show1">
						<ul class="clearfix">
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-1.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-2.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-3.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-4.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-5.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-6.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
						</ul>							
					</div>
					<div class="accordion-show2">
						<ul class="clearfix">
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-7.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿22222</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-8.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-9.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-10.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-11.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>


							</li>
							<li>
								<a class="infoma-bg-show" href="supervision_case_details.php" title="" target="_blank">
									<img src="../images/demo_images/12-12.jpg" alt="图1" />
								</a>
								<div class="infoma-wxh">
									<a href="supervision_case_details.php" title="">
										<img class="infoma-pic-wxh" src="../images/demo_images/head.png" alt="头像" />
										<span>杨宝睿</span>
										<del>北京建邦华庭西区</del>	
									</a>
								</div>
							</li>
						</ul>
					</div>					
				</div>
			</div>
		</div>
		<div class="four-cql">
			<div class="four-title">
				<div class="showcall-cql">
					<div class="four-title-main clearfix">
						<span class="four-sel-cql">联系我们</span>
						<span>权威报道</span>
						<span>关于我们</span>
						<span>加入我们</span>
					</div>
				</div>
			</div>
			<div class="four-content">
				<div class="callus content-show-cql">
					<div class="call-title">
						<div class="call-til">
							<em>免费预约监理</em>
							<var></var>
							<span>400-4267-518</span>
						</div>
					</div>
					<div class="call-con">
						<form action="">
							<div class="none-cql">
								<input type="text" class="len-cql" placeholder="小区 Community" style="border:0;">
								<input type="text" class="len-cql" placeholder="电话 Phone"  style="border:0;" id="phonee">
								<textarea cols="30" rows="10" placeholder="备注 Note"  style="border:0;" class="text-len-cql"></textarea>
								<input type="button" class="spaly-cql" style="border:0;" value="立即申请">
								<em></em>
								<span></span>
							</div>
						</form>
					</div>

				</div>
				<div class="report-cql">
					<div class="outer-report-cql clearfix" id="outerC">
						<div class="inner-report-cql clearfix">
							<a href="http://j.news.163.com/docs/99/2015031817/AL0QQ1G29001Q1G3.html" title="" target="_blank">
							 	<img src="../images/demo_images/reports2.jpg" alt="" title="">
							 	<span>装小蜜王志峰解析家装O2O</span>
							</a>
							<a href="http://www.pintu360.com/article/550b5bfaae8fc8cb4dafd489.html" title="" target="_blank">
							 	<img src="../images/demo_images/reports3.jpg" alt="" title="">
							 	<span>装小蜜王志峰解析家装O2O</span>
							</a>
							<a href="http://pe.hexun.com/2015-04-14/174940280.html" title="" target="_blank">
								<img src="../images/demo_images/reports4.jpg" alt="" title="">
								<span>装小蜜王志峰解析家装O2O</span>
							</a>
							<a href="http://www.donews.com/net/201503/2884734.shtm" title="" target="_blank">
							 	<img src="../images/demo_images/reports5.jpg" alt="" title="">
							 	<span>装小蜜王志峰解析家装O2O</span>
							</a>
							<a href="http://pe.hexun.com/2015-03-18/174173230.html" title="" target="_blank">
								<img src="../images/demo_images/reports3.jpg" alt="" title="">
								<span>装小蜜王志峰解析家装O2O</span>
							</a>
						</div>
						<div class="btn-cql">
							<div class="btn-left-cql"></div>
							<div class="btn-right-cql"></div>
						</div>
					</div>
				</div>
				<div class="aboutus-cql">
					<div class="aboutus-title-cql">
						<h3>谁是“装小蜜”</h3>
						<img src="../images/demo_images/aboutus.jpg" alt="" title="关于我们">
						<p>装小蜜”的愿景是希望让基础装修变的简单，让每个家庭可以花更多的时间在提升家的美观和品质上，而不是像现在一样，大部分精力花在和装修公司斗智斗勇上，最后还因为装修搞的遍体鳞伤。</p>
						<p>北京蜜蜂兄弟科技有限公司由国内互联网精英和资深装修监理人士一起创立，并获得国内顶级基金的风险投资，管家式监理理念的开创者，中国首家O2O装修监理服务网站，致力于用专业的第三方监理帮助用户搞定最头痛的装修，帮助业主避开装修中的各种陷阱。</p>
					</div>
				</div>
				<div class="joinus-cql">
					<h3>MASTER JOBS</h3>
					<span>高人/牛人/狂人/虚位以待</span>
					<p>想要最大发挥自己的热情和能力／希望得到一份很棒的工作／不希望仅仅为了生存而工作将你的简历发送至：gongshuai@zhuangxiaomi.com</p>
					<img src="../images/demo_images/addus.png" alt="" title="关于我们">
					<p id="pro">产品 / 策划 / 客服 / 运营 / 监理 / 设计</p>
				</div>
			</div>
		</div>
		<div class="returntop-cql">
			<ul>
				<li class="code-cql">
					<img src="../images/weixin.png" alt="" title="" class="lefthide-cql">
				</li>
				<li class="qq-cql">
					<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
					<div class="qqspan">
						<span>周一至周五</span>
						<span>9:00-21:00</span>
					</div>
				</li>
				<li class="return-cql" id="returnTop">
				</li>
			</ul>
		</div>
		<div class="wrap-friend-cql">
			<div class="friend-cql">
				<span>友情链接</span>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
				<a href="http://www.hunzhan123.com" title="">中国站长</a>
			</div>
		</div>
		<?php
			include("foot.html");
		?>		
	</body>
	<script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="../js/index_layout.js"></script>
	<script type="text/javascript" src="../js/common.js"></script>
	<script>
		 //点击马上预定跳转到底部orderinline-ql    
        $(".orderinline-ql").click(function() {
            var windowTop = $("body").scrollTop(2333);
        })     
	     // 为空弹窗
	    var reg = /^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
	    $(".spaly-cql").click(function() {
        	var phoner = $("#phonee").val();  
	        if(!($(".len-cql").val())) {
	            alert("小区名称不能为空");
	        } else  if(reg.test(phoner)) {
	        	alert("预约成功");
	        } else {
	        	alert("手机号码格式不正确");
	        }   
	    })
	</script>
</html>