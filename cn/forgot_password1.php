<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>忘记密码</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="../css/common.css" />
        <link rel="stylesheet" type="text/css" href="../css/login_register.css" />
    </head>
    <body class="wrap-jwy">
        <?php
            include("head.php");
        ?>
        <div class="forgot-box-jwy">
            <div class="clearfix">
                <span class="forgot-step-jwy">1.输入手机号码  ></span>
                <span>2.输入验证码  ></span>
                <span>3.重置密码</span>
            </div>
            <div class="forgot-con-jwy">
                <form action="" method="">
                <!-- 手机 -->
                    <div class="tel-num-jwy clearfix">
                        <label for="tel-jwy"></label>
                        <input type="text" name="" id="tel-jwy" placeholder="手机号" />
                    </div>
                    <!-- 验证码 -->
                    <div class="verification-jwy cleafix">
                        <label for="verification-jwy"></label>
                        <input type="text" name="" id="verification-jwy" placeholder="验证码" /> 
                        <img src="" alt="" title=""/>
                    </div>
                    <!-- 下一步 -->
                    <div class="forgot-sub-jwy">
                        <input type="submit" value="下一步" id="nextStepOne-jwy" />
                    </div>
                </form>
            </div>
        </div>
        <?php
            include("foot.html");
        ?>
    </body>
    <script src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script src="../js/login_register.js" type="text/javascript"></script>
    <script type="text/javascript">   
    // 电话失焦聚焦
    // 第一个参数需要获取的类名，第二个参数为聚焦的样式，第三个样式为失焦的样式
    // telBg开头为一个人的图片，verBg开头为一个锁的图片
        telBgJwy(".tel-num-jwy", telBgFous, telbgBlur);
    // 验证码失焦聚焦
        telBgJwy(".verification-jwy", verBgFous, verBgBlur );
    </script>
</html>