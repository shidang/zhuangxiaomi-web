<?php
    session_start();
    include("../php/include.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>装小蜜家装文章列表</title>
    <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
    <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <div class="wrap">
        <?php
            include("head.php");
        ?>
    </div>
    <div class="boxs-nyh" width="100%" height="100%">
        <div class="wrap-nyh">
            <div class="ranking-nyh clearfix">
                <ul>
                    <li class="ranking-top1">周排行TOP1</li>
                    <li class="ranking-top2">周排行TOP2</li>
                    <li class="ranking-top3">周排行TOP3</li>
                    <li class="ranking-top4">周排行TOP4</li>
                    <li class="ranking-top5">周排行TOP5</li>
                </ul> 
                <img src="../images/demo_images/ranking-top1.png" alt="" class="top1-nyh"> 
                <div class="header-nyh">
                   <div class="box-llone">
                        <ins class="header-english-nyh">
                            <span>WEEK</span>
                            <span>OF</span>
                            <span>RANKING</span>
                        </ins>
                        <em class="top1-logo-nyh"></em>
                        <h2>装小蜜带您识别</h2>
                        <h2>家装监控的12个误区</h2>
                        <p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。</p>
                        <h3 class="label-nyh">
                            <span>标签：</span>
                            <span>监理</span>
                            <span>家装</span>
                            <span>误区</span>
                            <span>装小蜜</span>
                        </h3>
                    </div>
                    <div class="box-lltwo">
                        <ins class="header-english-nyh">
                            <span>DARK</span>
                            <span>OF</span>
                            <span>SECRET</span>
                        </ins>
                        <em class="top1-logo-nyh"></em>
                        <h2>装小蜜带您了解</h2>
                        <h2>家装不为人知的秘密</h2>
                        <p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。</p>
                        <h3 class="label-nyh">
                            <span>标签：</span>
                            <span>监理</span>
                            <span>家装</span>
                            <span>误区</span>
                            <span>装小蜜</span>
                        </h3>
                    </div>
                    <div class="box-llthree">
                        <ins class="header-english-nyh">
                            <span>DURING</span>
                            <span>OF</span>
                            <span>OPERATION</span>
                        </ins>
                        <em class="top1-logo-nyh"></em>
                        <h2>装小蜜带您操作</h2>
                        <h2>家装监理的整个过程</h2>
                        <p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。</p>
                        <h3 class="label-nyh">
                            <span>标签：</span>
                            <span>监理</span>
                            <span>家装</span>
                            <span>误区</span>
                            <span>装小蜜</span>
                        </h3>
                    </div>
                    <div class="box-llfour">
                        <ins class="header-english-nyh">
                            <span>SEE</span>
                            <span>THE</span>
                            <span>EFFECTS</span>
                        </ins>
                        <em class="top1-logo-nyh"></em>
                        <h2>装小蜜带您观看</h2>
                        <h2>家装完成的所有效果</h2>
                        <p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。</p>
                        <h3 class="label-nyh">
                            <span>标签：</span>
                            <span>监理</span>
                            <span>家装</span>
                            <span>误区</span>
                            <span>装小蜜</span>
                        </h3>
                    </div>
                    <div class="box-llfive">
                        <ins class="header-english-nyh">
                            <span>DECISION</span>
                            <span>TO</span>
                            <span>UNDERSTAND</span>
                        </ins>
                        <em class="top1-logo-nyh"></em>
                        <h2>装小蜜带您懂得</h2>
                        <h2>家装监控的所有明确</h2>
                        <p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把关，选择合适的装修公司。</p>
                        <h3 class="label-nyh">
                            <span>标签：</span>
                            <span>监理</span>
                            <span>家装</span>
                            <span>误区</span>
                            <span>装小蜜</span>
                        </h3>
                    </div>
                </div> 
            </div>
            <div class="sort-search-nyh clearfix">
                <a class="release" href="">发布时间</a>
                <span>|</span>
                <a href="">按人气</a>
                <span>|</span>
                <a href="">按推荐</a>
                <span>|</span>
                <a href="">按评论</a>
                <label for="text"></label>
                <input type="text" name="text" placeholder="热门搜索：案例监理"/>
            </div>
            <?php 
                $sql = mysql_query("SELECT * FROM article_list");
                while($row = mysql_fetch_assoc($sql)) {
            ?>        
            <dl class="content-nyh clearfix">
                <dt>
                    <a href="supervision_case.php">
                        <img src="<?php echo $row["pic_url"]; ?>" alt="" />
                    </a>
                </dt>
                <dd>                    
                    <span class="pic-nyh"></span>
                    <a href="article_details.php?aid=<?php echo $row['id'];?>" title="管家式监理2015年度精细化家装监理">
                        <h3>【品牌活动】管家式监理2015年度精细化家装监理</h3>
                        <img src="../images/demo_images/icon-nyh.png" height="36" width="36" alt="" />
                        <span class="professional-nyh"><?php echo $row["autor"]; ?>/编辑</span>
                        <p><?php echo $row["content"]; ?></p>
                    </a>
                </dd>
                <dd>
                    <div class="content-foot-nyh">
                        <span><?php echo date("Y-m-d", strtotime($row["date"]));?></span>
                        <span><?php echo date("H:m", strtotime($row["date"])); ?></span>
                        <span>评论</span>
                        <span class="foot-digital-nyh">24</span>
                        <span>推荐/</span>
                        <span class="foot-digital-nyh">36</span>
                        <span>人气/</span>
                        <span class="foot-digital-nyh"><?php echo $row["click"] ?></span>
                    </div>
                </dd>
            </dl>
            <?php } ?>
        </div>           
    </div>
    <div class="returntop-cql">
        <ul>
            <li class="code-cql">
                <img src="../images/weixin.png" alt="" title="" class="lefthide-cql">
            </li>
            <li class="qq-cql">
                <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                <div class="qqspan">
                    <span>周一至周五</span>
                    <span>9:00-21:00</span>
                </div>
            </li>
            <li class="return-cql" id="returnTop">
            </li>
        </ul>
    </div>
    <?php
        include("foot.html");
    ?>   
</body>
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript">
        var i = 0;
        setInterval(function(){
            i += 1;
            if (i > 5) {
                i = 1;
            }
            $(".top1-nyh").attr("src","../images/demo_images/content-" + i + ".png");
            $(".header-nyh").children().eq(i-1).css({"display":"block"}).siblings().css({"display":"none"});
            $(".ranking-nyh li").eq(i-1).stop(true, true).animate({
                "width" : "210px"
            }, 1000).siblings().stop(true, true).animate({
                "width" : "10px"
            }, 1000)
            $(".ranking-nyh img").click(function() {
                if(i == 1) {
                    $(".header-nyh").children(".box-llone").css({"display":"block"}).siblings().css({"display":"none"});
                }
                if(i == 2) {
                    $(".header-nyh").children(".box-lltwo").css({"display":"block"}).siblings().css({"display":"none"});
                }
                if(i == 3) {
                    $(".header-nyh").children(".box-llthree").css({"display":"block"}).siblings().css({"display":"none"});
                }
                if(i == 4) {
                    $(".header-nyh").children(".box-llfour").css({"display":"block"}).siblings().css({"display":"none"});
                }
                if(i == 5) {
                    $(".header-nyh").children(".box-llfive").css({"display":"block"}).siblings().css({"display":"none"});
                }
            })
        }, 2000)
    </script>
</html>