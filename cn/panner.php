<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>装修——我们结伴而行</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="../css/common.css" />
        <link rel="stylesheet" type="text/css" href="../css/page.css" />
    </head>
    <body>
        <div class="wrap-show-lll">
            <a href="index.php" title="" class="logopic">
                <img src="../images/logo1.png" alt="" />
            </a>
            <div class="navs-xh">
                <a href="supervision_service.php" title=""> 监理服务 /</a>
                <a href="supervision_team.php" title=""> 监理团队 /</a>
                <a href="supervision_case.php" title=""> 监理案例 /</a>
                <a href="article_list.php" title=""> 装修秘密 /</a>
                <a href="supervision_activity.php" title=""> 蜜材联盟</a>
            </div>
            <div class="infomation-wxh">
                <p>您知道吗？当您准备装修时，刚坑过我的施工队可能正在接触您，信誓旦旦忽悠您签满是陷阱的合同，我们可能看过同样的装修日记，逛了同样的主材店，也同样和各种人唇枪舌战；在装修时我们总在做着类似的事情，掉入同样的陷阱，被同样的人当成了冤大头宰了又宰……我们一样的无助，孤单！这个时候我总在想，如果有个地方可以将装修的人集结起来该多好，终于有一天我醒来，不想再用劣质又昂贵的建材，不想再听工长说：你不懂！我想对所有的不公平说不，我多想和那个同样醒来的你打个招呼！或许我们可以结伴而行互相帮助！所以，我在这儿等你……</p>
            </div>
        </div>
    </body>
    <script type="text/javascript"  src="../js/jquery-1.11.3.min.js"></script>
</html>