<?php 
    include("../php/include.php"); 
?>
<div class="nav-jwy">
    <div class="logo-jwy"></div>
    <div class="nav-list" id="nav-list">
        <span class="line-jwy" id="line-jwy"></span>
        <a href="index.php" title="首页">首页</a>
        <a href="supervision_service.php" title="监理服务">监理服务</a>
        <a href="supervision_team.php" title="监理团队">监理团队</a>
        <a href="supervision_case.php" title="监理案例">监理案例</a>
        <a href="article_list.php" title="装修秘密">装修秘密</a>
        <a href="supervision_activity.php" title="蜜材联盟">蜜材联盟</a>
    </div>
    <div class="login clearfix" id="login">
        <a href="login.php" title="登陆">登录</a>
        <a href="register.php" title="注册">注册</a>
    </div>
    <div class="login-after" id="login_after" style="display:none">
        <a href="personal_center.php?bid=<?php echo  $_SESSION["uid"];?>">
            你好，<?php echo $_SESSION["username"];?>
        </a>
    </div>
</div> 
<script type="text/javascript">
    var login = document.getElementById("login");
    var loginAfter = document.getElementById("login_after");
    if(<?php echo $_SESSION["uid"] ?>){
        login.style.display = 'none';
        loginAfter.style.display = "block";
    } 
    // else {
    //     console.log("a");
    //     login.style.display = 'block';
    //     loginAfter.style.display = "none";
    // }
</script> 