<?php 
    session_start();
    include("../php/include.php");    
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>个人中心—装小蜜</title>
		<link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
		<link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="../css/common.css" />
		<link rel="stylesheet" type="text/css" href="../css/login_register.css" />
		<script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
		<style>
			input::-ms-clear {
    			display: none;
			}
			html {
				background-color: #e9e9e9;
			}
		</style>
	</head>
	<body>
		<div class="wrap">
			<!-- 头部信息 -->
			<?php
            	include("head.php");
            	$bid = $_SESSION["uid"];
			    $sql = mysql_query("SELECT * FROM user_info WHERE id=$bid");
			    $row = mysql_fetch_assoc($sql);
        	?>
		</div>
		<!-- 主体信息显示 -->
		<div class="main-wxh">
			<div class="content-left-wxh">
				<div class="head-portrait-wxh">	
					<img src="../images/demo_images/pic.png" alt="" />
				</div>
				<span><?php echo  $row["username"];?>	</span>			
				<em></em>
				<div class="con-bottom-wxh">
					<a class="sina-wxh" href="" title=""></a>
					<!-- <a class="qq-wxh" href="" title=""></a> -->
					<a class="qq-wxh" target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
					<a class="weixin-wxh" href="" title=""></a>
					<a class="dou-wxh" href="" title=""></a>
				</div>
				<div class="btn-wxh">
					<ins id="btn-wxh">账号设置</ins>
				</div>
				<div class="dabai-banner-wxh">
					<a href="http://baike.baidu.com/link?url=wRUWHTteYFfjYNkyAgpUVkyAHe1s_4erhnlls7xiZpPKpUigr-f7SoBojAuNw1M07g2M2HdFLm0ZgLxQA7NFy9tTfSqT53T1F5Ss0b5k3EW" title="">
						<img src="../images/demo_images/dabai-banner.png" alt="" />
					</a>
				</div>
			</div>
			<div class="content-right-wxh">
				<div class="tit-wxh" id="tit-wxh">
					<h2 class="select">我的工地</h2>
					<h2>我的评论<span>5</span></h2>
					<h2>账号设置</h2>
				</div>
				<div class="con-wxh" id="con-wxh">
					<!-- 我的工地 -->
					<div class="mysite-wxh" id="mysite-wxh">
						<div class="mysite-banner-wxh">
							<div class="bigpic-show-wxh">
								<a href="" title="">
									<img src="../images/demo_images/pic-show.png" alt="" />
								</a>
							</div>
							<div class="smallpic-show-wxh">
								<div class="smallpic-wxh">
									<a href="" title="">
										<img src="../images/demo_images/pic-show.png" alt="" />
									</a>
								</div>
								<div class="smallpic-wxh">
									<a href="" title="">
										<img src="../images/demo_images/pic-show.png" alt="" />
									</a>
								</div>
								<div class="smallpic-wxh">
									<a href="" title="">
										<img src="../images/demo_images/pic-show.png" alt="" />
									</a>
								</div>
								<div class="shadow-wxh">
									<a href="" title="">
										<img src="../images/demo_images/pic-shadow.png" alt="" />
									</a>
								</div>
							</div>
							<dl class="info">
								<dt class="info-tit">
									<em>北京麒麟瑞景别墅</em>
									<span>已完工</span>
								</dt>
								<dd class="info-con">
									<span>面积：</span>
									<em>135m<sup>2</sup>
									</em>
								</dd>
								<dd class="info-con">
									<span>决算：</span>
									<em>23万元（预算：36万元）</em>
								</dd>
								<dd class="info-con">
									<span>实际工期：</span>
									<em>32天（预计工期：45天）</em>
								</dd>
								<dd class="info-con">
									<span>业主：</span>
									<em>李女士</em>
								</dd>
								<dd class="info-con">
									<span>工长：</span>
									<em>张元培</em>
								</dd>
								<dd class="info-con">
									<span>工程评价：</span>
									<i></i>
									<i></i>
									<i></i>
									<i></i>
									<i></i>
									<em class="score">8.6分</em>
									<a href="supervision-case-details.php">详情 ></a>
								</dd>
							</dl>
						</div>
						<h2>工地的相关负责人</h2>
						<div class="mysite-info-wxh">
							<dl>
								<dt>
									<a href="supervision_team.php" title="">
										<img src="../images/demo_images/pic.png" alt="" />
									</a>
								</dt>
								<dd>
									<h3>林云朗</h3>
									<em>监理</em>
									<span>持证：监理工程师</span>
									<span>从业：10年以上</span>
									<span>电话：135-1024-8059</span>
								</dd>
							</dl>							
							<dl>
								<dt>
									<a href="supervision_team.php" title="">
										<img src="../images/demo_images/pic.png" alt="" />
									</a>
								</dt>
								<dd>
									<h3>林云朗</h3>
									<em>监理</em>
									<span>持证：监理工程师</span>
									<span>从业：10年以上</span>
									<span>电话：135-1024-8059</span>
								</dd>
							</dl>
							<dl>
								<dt>
									<a href="supervision_team.php" title="">
										<img src="../images/demo_images/pic.png" alt="" />
									</a>
								</dt>
								<dd>
									<h3>林云朗</h3>
									<em>监理</em>
									<span>持证：监理工程师</span>
									<span>从业：10年以上</span>
									<span>电话：135-1024-8059</span>
								</dd>
							</dl>
						</div>
						<div class="mysite-zixun-wxh">
							<dl>
								<dt>
									<a href="supervision_team.php" title="">
										<img src="../images/demo_images/pic.png" alt="" />
									</a>
								</dt>
								<dd>
									<h3>咨询热线</h3>
									<span>135-1024-8059</span>
								</dd>
							</dl>
						</div>
					</div>
					<!-- 我的评论 -->
					<div class="mycomments-wxh con-select" id="mycomments-wxh">
						<div class="mycomments-con-jwy clearfix">
						<!-- 文章缩略部分 -->
							<dl>
								<dt class="clearfix">
									<a href="article_details.php" title="">
										<img src="../images/demo_images/12-11.jpg" alt="" title=""/>	
									</a>
								</dt>
								<dd>
									<h2>
										<a href="article_details.php" tltle="">一起带您识别家装监理的12个误区</a>
									</h2>
									<p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司</p>
									<span>2015-04-12</span>
									<q>19:46</q>
									<del>(90)</del>
									<a href="">全部评论</a>
								</dd>
							</dl>
							<!-- 本文的评论 -->
							<div class="comment-con-jwy">
								<span class="jiantou"></span>
								<dl class="clearfix">
									<dt>
										<b>张曼丽</b>
										<span class="vip"></span>
										<q>35F</q>
									</dt>
									<dd>
										<p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司。</p>
										<span>45分钟前</span>
										<em>赞（15）|回复（12）</em>
									</dd>
								</dl>
							</div>
						</div>
						<div class="mycomments-con-jwy clearfix">
						<!-- 文章缩略部分 -->
							<dl>
								<dt class="clearfix">
									<a href="article_details.php" title="">
										<img src="../images/demo_images/12-11.jpg" alt="" title=""/>	
									</a>
								</dt>
								<dd>
									<h2>
										<a href="article_details.php" tltle="">一起带您识别家装监理的12个误区</a>
									</h2>
									<p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司。装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司</p>
									<span>2015-04-12</span>
									<q>19:46</q>
									<del>(90)</del>
									<a href="">全部评论</a>
								</dd>
							</dl>
							<!-- 本文的评论 -->
							<div class="comment-con-jwy">
								<span class="jiantou"></span>
								<dl class="clearfix">
									<dt>
										<b>张曼丽</b>
										<span class="vip"></span>
										<q>35F</q>
									</dt>
									<dd>
										<p>装小蜜提供不高于10家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮你从源头把关，选择合适的专修公司。</p>
										<span>45分钟前</span>
										<em>赞（15）|回复（12）</em>
									</dd>
								</dl>
							</div>
						</div>
						<form action="" method="">
							<label for="mycomments-submit">更多评论</label>
							<input class="mycomments-submit-jwy" type="submit" id="mycomments-submit" placeholder="更多评论"/>
						</form>
					</div>
					<!-- 帐号设置 -->
					<div class="accountsettings-wxh con-select" id="accountsettings-wxh">
						<form action="" method="">
							<!-- 第一部分 -->
							<div class="account-setup-jwy">
								<div class="account-setup-left">
									<img src="../images/demo_images/pic.png" alt="" title=""/>
									<label for="head-pic">修改头像</label>
									<input type="file" id="head-pic" class="headpic" />
								</div>
								<ul>
									<li>
										<label for="username">修改</label>
										<input type="text" id="username" placeholder=""/>
										<span>姓名：</span>
									</li>
									<li>
										<label for="name">修改</label>
										<input type="text" id="name" placeholder=""/>
										<span>昵称：</span>
									</li>
									<li>
										<label for="sex">修改</label>
										<input type="text" id="sex" placeholder=""/>
										<span>性别：</span>
									</li>
									<li>
										<label for="brith">修改</label>
										<input type="text" id="brith" placeholder=""/>
										<span>生日：</span>
									</li>
								</ul>
							</div>
							<!-- 安全信息部分 -->
							<div class="safety-information">
								<div class="safety-head">
									<span>安全设置</span>
									<var>65分</var>
									<q>安全等级</q>
								</div>
								<!-- 登录密码 -->
								<div class="login-pass" id="loginpassjwy">
									<del></del>
									<span>登录密码：</span>
									<em>中级</em>
									<q>密码长度为6-30个字符，建议由字母数字符号组成，不宜与注册手机号相同。</q>
								</div>
								<ul class="login-pass-con" id="loginpasscon">
									<li>
										<label for="oldpass">当前密码</label>
										<input type="text" id="oldpass" />
									</li>
									<li>
										<label for="newpass">新密码</label>
										<input type="text" id="newpass" />
									</li>
									<li>
										<label for="surepass">确认密码</label>
										<input type="text" id="surepass" />
									</li>
									<li class="btn-pass">
										<label for="submit" class="lab">确定</label>
										<input type="button" class="btn" value="取消" />
										<input type="submit" id="submit" class="sub" />
									</li>
								</ul>
								<!-- 邮箱绑定 -->
								<div class="login-pass" id="loginmailjwy">
									<del></del>
									<span>邮箱绑定：</span>
									<em>8393*****qq.com</em>
									<q>可以通过邮箱登录和找回密码。</q>
								</div>
								<ul class="login-pass-con" id="loginmailcon">
									<li>
										<label for="oldmail">原邮箱</label>
										<input type="text" id="oldmail" />
									</li>
									<li>
										<label for="newmail">新邮箱</label>
										<input type="text" id="newmail" />
									</li>
									<li class="btn-pass">
										<label for="submit-mail" class="lab">确定</label>
										<input type="button" class="btn" value="取消" />
										<input type="submit" id="submit-mail" class="sub" />
									</li>
								</ul>
							</div>
							<!-- 帐号绑定部分 -->
							<div class="safety-information">
								<div class="safety-head">
									<span>账号绑定</span>
								</div>
								<ul class="account-binding-jwy">
									<li>
										<span class="account-binding-select"></span>
										<q class="account-binding-sina"></q>
										<ins>微博账号绑定：</ins>
										<del>Angel****@gmail.com  </del>
										<label for="binding-sub">解除绑定</label>
										<input type="submit" id="binding-sub" />	
									</li>
									<li>
										<span class="account-binding-select"></span>
										<q class="account-binding-qq"></q>
										<ins>QQ账号绑定：</ins>
										<del>839319729  </del>
										<label for="binding-sub">解除绑定</label>
										<input type="submit" id="binding-sub" />	
									</li>
									<li>
										<span class="account-binding-select"></span>
										<q class="account-binding-weixin"></q>
										<ins>微信账号绑定：</ins>
										<del>JWY2015  </del>
										<label for="binding-sub">解除绑定</label>
										<input type="submit" id="binding-sub" />	
									</li>
									<li>
										<span class=""></span>
										<q class="account-binding-dou"></q>
										<ins>其他账号绑定：</ins>
										<del></del>
										<label for="binding-sub" class="no-binding">点击绑定</label>
										<input type="submit" id="binding-sub" />	
									</li>
								</ul>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- 尾部信息 -->
		<?php
			include("foot.html");
		?>		
	</body>
	<script type="text/javascript" src="../js/common.js"></script>
	<script type="text/javascript" src="../js/login_register.js"></script>
	<script type="text/javascript">
		// 切换内容部分
        // 获取title的标签，然后根据点击的数组下标，改变内容的隐藏和显示，在类名中添加on-select则为隐藏部分。
        // 作者JWY
        var tits = document.getElementById('tit-wxh').getElementsByTagName('h2');
        var mysiteWxh = document.getElementById("mysite-wxh");
        var mycommentsWxh = document.getElementById("mycomments-wxh");
        var accountsettingsWxh = document.getElementById("accountsettings-wxh");
        tits[0].onclick = function() {
            tits[0].className = "select";
            tits[1].className = "";
            tits[2].className = "";
            mysiteWxh.className = "mysite-wxh";
            mycommentsWxh.className = "mycomments-wxh con-select";
            accountsettingsWxh.className = "accountsettings-wxh con-select";
        }
        tits[1].onclick = function() {
            tits[0].className = "";
            tits[1].className = "select";
            tits[2].className = "";
            mysiteWxh.className = "mysite-wxh con-select";
            mycommentsWxh.className = "mycomments-wxh";
            accountsettingsWxh.className = "accountsettings-wxh con-select";
        }
        tits[2].onclick = function() {
        	show();
        }
        function show() {
            tits[0].className = "";
            tits[1].className = "";
            tits[2].className = "select";
            mysiteWxh.className = "mysite-wxh con-select";
            mycommentsWxh.className = "mycomments-wxh con-select";
            accountsettingsWxh.className = "accountsettings-wxh";
        }
        var btn_wxh = document.getElementById("btn-wxh");
        btn_wxh.onclick = function(){
        	show();
        }       
	</script>
</html>