<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>监理服务</title>
    <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
    <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/page.css">
</head>
<body>
    <div class="wrap">
        <?php
            include("head.php");
        ?>
    </div>
    <div class="service">
        <div class="main">
            <div class="project">
                <div class="title">
                    <h1>家装监理</h1>
                    <p>6大项 150条检验标准 全程施工管家式监理服务</p>
                </div>
                <div class="marginbox clearfix">
                    <div class="initial">
                        <strong>15</strong>
                        <em>类</em>
                        <span>初期审核</span>
                    </div>
                    <div class="spotTrade">
                        <strong>16</strong>
                        <em>类</em>
                        <span>现场交底</span>
                    </div>
                    <div class="shelterPoj">
                        <strong>26</strong>
                        <em>类</em>
                        <span>隐蔽工程</span>
                    </div>
                    <div class="interimPoj">
                        <strong>46</strong>
                        <em>类</em>
                        <span>中期工程</span>
                    </div>
                    <div class="materialAccept">
                        <strong>27</strong>
                        <em>类</em>
                        <span>材料验收</span>
                    </div>
                    <div class="overAccept">
                        <strong>20</strong>
                        <em>类</em>
                        <span>竣工验收</span>
                    </div>
                </div>
                <div class="poj-con clearfix">
                    <span></span>
                    <div class="initial-con">
                        <h2>初期审核 - 15类</h2>
                        <p class="drawing"><strong>图纸审核：</strong>用专业的角度，帮您从源头审核装饰公司或施工队提供的图纸，提供合理建议。</p>
                        <p class="mon"><strong>报价审核：</strong>源头把关，彻底杜绝报价不合理，从前期避免报价陷阱虚高报价。</p>
                        <div class="initial-list">
                            <ul class="clearfix">
                                <li>单价合理</li>
                                <li>无重复收费</li>
                                <li>工艺说明详细</li>
                                <li>材料使用得当</li>
                                <li>无多余项目</li>
                                <li>收费明确</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li> 
                            </ul>
                        </div>
                    </div>
                    <div class="spottrade-con">
                        <h2>现场交底 - 16项</h2>
                        <p>仔细检查房屋基本情况，业主与监理、设计师，三方确定房屋监理方案。</p>
                        <div class="spottrade-list">
                            <ul class="clearfix">
                                <li>单价合理</li>
                                <li>无重复收费</li>
                                <li>工艺说明详细</li>
                                <li>材料使用得当</li>
                                <li>无多余项目</li>
                                <li>收费明确</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li> 
                            </ul>
                        </div>
                    </div>
                    <div class="shelterpoj-con">
                        <h2>现场交底 - 26项</h2>
                        <p>监理会通过专业的技能为业主从拆除新建项目、水路改造、电路改造、隐蔽工程验收四个节点层层把好质量关。</p>
                        <div class="shelterpoj-list">
                            <ul class="clearfix">
                                <li>单价合理</li>
                                <li>无重复收费</li>
                                <li>工艺说明详细</li>
                                <li>材料使用得当</li>
                                <li>无多余项目</li>
                                <li>收费明确</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li>
                            </ul>
                        </div>
                    </div>
                    <div class="interimpoj-con">
                        <h2>中期工程 - 46项</h2>
                        <p>由业主与监理、设计师、施工负责人三方参与，对工程材料、设计、工艺质量46项进行整体验收，合格后签字确认。</p>
                        <div class="interimpoj-list">
                            <ul class="clearfix">
                                <li>单价合理</li>
                                <li>无重复收费</li>
                                <li>工艺说明详细</li>
                                <li>材料使用得当</li>
                                <li>无多余项目</li>
                                <li>收费明确</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li>
                            </ul>
                        </div>
                    </div>
                    <div class="materialaccept-con">
                        <h2>材料验收 - 27类</h2>
                        <p>装修过程中最容易出现纠纷的就是材料，施工方以次充好，或者合同中没有详细准确的约定都会造成纠纷。确定合同的时候必须明确规定各种材料的详细信息，同时在材料进场的时候监理人员会对墙体材料、地面材料、装饰线、顶部材料、紧固件、连接件、胶粘剂等诸类别材料按合同明确的材料标准进行认真验收。</p>
                        <div class="materialaccept-list">
                            <ul class="clearfix">
                                <li>单价合理</li>
                                <li>无重复收费</li>
                                <li>工艺说明详细</li>
                                <li>材料使用得当</li>
                                <li>无多余项目</li>
                                <li>收费明确</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li>
                            </ul>
                        </div>
                    </div>
                    <div class="overaccept-con">
                        <h2>竣工验收 - 20项</h2>
                        <p>对该项目20项节点，是否符合规划设计要求以及建筑施工和设备安装质量进行全面检验。</p>
                        <div class="overaccept-list">
                            <ul class="clearfix">
                                <li>单价合理</li>
                                <li>无重复收费</li>
                                <li>工艺说明详细</li>
                                <li>材料使用得当</li>
                                <li>无多余项目</li>
                                <li>收费明确</li>
                                <li>报价与方案相符</li>
                                <li>图纸全面</li>
                                <li>符合图纸规范</li>
                                <li>材料使用符合规范</li>
                                <li>拆改无违规事项</li>
                                <li>无安全隐患</li>
                                <li>方案与原结构相对应</li>
                                <li>方案与原结构相对应</li>
                                <li>造型方案可实现</li>
                                <li>空间布置无使用障碍</li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="program">
            <h2>常见问题</h2>
            <div class="program-list">
                <div class="supervisor">
                    <h3>家庭专修为什么要请监理？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况
                    </p>
                </div>
                <div class="detection">
                    <h3>如何预约检测？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
                <div class="professionassess">
                    <h3>为什么要进行专业的车辆评估？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
                <div class="offer">
                    <h3>大搜车给车辆报价吗？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
                <div class="satisfaction">
                    <h3>如果对竞价价格不满意怎么办？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
                <div class="safety">
                    <h3>大搜车如何保证交易和手续的安全性？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
                <div class="make-over">
                    <h3>转让需要带哪些东西？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
                <div class="offerarea">
                    <h3>支持提供上门检测的区域有哪些？<span></span></h3>
                    <p>装小蜜提供不高于数家装修公司的初步评测，不高于3家装修公司实地工艺评测服务，帮您从源头把装小蜜公司会导致设计的效果完全无法施预算不详成业主实际花费远远超出计划出现充好的情况</p>
                </div>
            </div>
        </div>
        <!-- <div class="top"></div>
        <div class="two-dimension"></div> -->
    </div>
    <div class="returntop-cql">
        <ul>
            <li class="code-cql">
                <img src="../images/weixin.png" alt="" title="" class="lefthide-cql">
            </li>
            <li class="qq-cql">
                <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                <div class="qqspan">
                    <span>周一至周五</span>
                    <span>9:00-21:00</span>
                </div>
            </li>
            <li class="return-cql" id="returnTop">
            </li>
        </ul>
    </div>
    <?php
        include("foot.html");
    ?>     
</body>
    <script src="../js/jquery-1.11.3.min.js" charset="utf-8" type="text/javascript"></script>
    <script src="../js/supervision.js" charset="utf-8" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript" charset="utf-8"></script>
</html>