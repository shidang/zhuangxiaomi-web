<?php 
    session_start();
    include("../php/include.php");
    $aid = $_GET["aid"];
    $sql = mysql_query("SELECT * FROM article_list WHERE id=$aid");
    $row = mysql_fetch_assoc($sql);
    $row["click"] = $row["click"] + 1;
    mysql_query("UPDATE article_list SET click=$row[click] WHERE id=$aid");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>装小蜜家装监理-互联网家装监理服务</title>
    <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
    <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <div class="wrap">
        <?php
            include("head.php");
        ?>
    </div>
    <div class="wrapx-nyh">
        <div class="path-nyh">
            <a href="index.php">首页</a>
            <span>＞</span>
            <a href="article_list.php">装修秘密</a>
            <span>＞</span>
            <a href="article_details.php">装修详情</a>
        </div>
        <div class="headerx-nyh">
            <div class="headxline-nyh clearfix">
                <span></span>
                <h2>管家式监理2015年度精细化家装监理</h2>
            </div>
            <div class="time-information">
                <span>2015-04-15</span>
                <span>19:46/</span>
                <span class="digital-nyh"><?php echo $row["click"] ?></span>
                <span>人气/</span>
                <span class="digital-nyh"><?php echo $row["hot"] ?></span>
                <span>推荐/</span>
                <span class="digital-nyh">24</span>
                <span>评论</span>
            </div>
        </div>
        <div class="contentx-nyh">
            <?php echo $row["content-detail"]; ?>
            <!-- <p>装修工程不是现成的产品无论施工队还是装修公司，装修都是在装饰设计和施工过程中形成的。试想，装修前，装修企业内部的质检（经常误称为监理），怎么可能指出自家预算中的漏洞，帮助消费者签订公平、透明的装修合同呢？装修过程中装修企业内部的质检是自己监督自己，运动员兼作裁判员的监督机制消费者又会获得几层的知情权装修后如果装修企业自己出现重大问题，又怎么可能代理消方监理才能公正客观的处理装修中的问题，才能真正地保障业主的权益。</p>
            <img src="../images/demo_images/pic1-nyh.png" alt="">
            <p>管家式监理不同于传统的监理，传统监理只负责工程质量的验收，装小蜜更关注装修的整体方案，除了监理工程质量之外帮助业主审核设计图纸，审核工程预算，跟踪工程进度，装修监理帮助业主审核装修方案和报价，防止高估冒算和中期无限度任意加项加价，使用户支付合理的装修价位提供资金托管及全程咨询服务等，全方位解决业主的装修烦恼。</p>
            <img src="../images/demo_images/pic2-nyh.png" alt="">
            <p>监理在“第三方”的立场上，公正地为业主服务，不放弃原则也不刻意找茬儿，在整个装修过程中，装修监理着力协调好业主与装饰公司双方的关系，监帮结合，每一次到现场会详细记录填写监理日记及相关验收表格。现场会将现场情况通过微信的方式向业主汇报，以方便业主及时掌握现场进度及情况。使业主与装饰公司的预定目标得以完美实现。装修监理帮助业主审核装修方案和报价，防止高估冒算和中期无限度任意加项加价，使用户支付合理的装修价位，这也是一种省钱。</p>
            <p>装修监理公司帮助业主检验装饰材料，防止假冒伪劣材料进入施工现场，使用户物有所值的真货进行装修，花同样的钱不用假料，用真材实料，也是一种省钱的措施。尤其，有装修监理，业主可以高枕无忧，节省出的时间和精力以及快乐的装修体验，更是价值不菲，所以说请装修监理不是在花钱，而是在省钱！</p> -->
            <div class="praise-nyh">
                <div class="praise-icon">0</div>
                <div class="share-nyh">
                    <a href="" title="分享到新浪微博" class="weibo-nyh"></a>
                    <a href="" title="分享到腾讯QQ" class="qq-nyh"></a>
                    <a href="" title="分享到微信" class="wechat-nyh"></a>
                    <a href="" title="分享到豆瓣网" class="dou-nyh"></a>
                </div>
            </div>
        </div>
        <div class="comments-nyh">
            <div id="SOHUCS"></div>
        </div>
    </div>
    <div class="returntop-cql">
        <ul>
            <li class="code-cql">
                <img src="../images/weixin.png" alt="" title="" class="lefthide-cql">
            </li>
            <li class="qq-cql">
                <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                <div class="qqspan">
                    <span>周一至周五</span>
                    <span>9:00-21:00</span>
                </div>
            </li>
            <li class="return-cql" id="returnTop">
            </li>
        </ul>
    </div>
   <?php
        include("foot.html");
    ?>    
</body>
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script charset="utf-8" type="text/javascript" src="http://changyan.sohu.com/upload/changyan.js
    "></script>
    <script type="text/javascript">
        window.changyan.api.config({
            appid: 'cys38cm5U',
            conf: 'prod_c870d38ab7d12241bd4be213c98cea53'
        }); 
        var obj_nyh = true;
        $(".praise-nyh .praise-icon").click(function(){
            if (obj_nyh) {
                var i = parseInt($(this).html()) + 1;
                $(this).html(i);
                obj_nyh = false;
            } else {
                alert("点过赞了！")
            }            
        })
    </script>
</html>