<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>监理团队</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" href="../css/common.css">
        <link rel="stylesheet" href="../css/page.css">
    </head>
    <body>
        <div class="wrap">
           <?php
                include("head.php");
            ?>
        </div> 
        <div class="svteam">
            <div class="main">
                <div class="title">
                    <h1>监理团队</h1>
                    <p>国内一线监理团队 持证上岗 管家式监理保障您的家装质量</p>
                </div>
                <div class="people clearfix">
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg.png" heigh2t="112" width="112" alt="" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg2.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg2.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg2.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg2.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl>
                        <dt>
                            <img src="../images/demo_images/userImg.png" alt="2" />
                        </dt>
                        <dd>
                            <h3>总工程师-范晓东</h3>
                            <p>从事装修行业40年 资深家装监理</p>
                        </dd>
                    </dl>
                    <dl class="certificate">
                        <dt></dt>
                        <dd>监理资质证书</dd>
                    </dl>
                </div>
            </div>
            <div class="wrap-pic">
                <img src="../images/demo_images/show.png" alt="cer" title="">
                <button>X</button>
            </div>
        </div>
        <?php
            include("foot.html");
        ?>    
    </body>
    <script src="../js/jquery-1.11.3.min.js" charset="utf-8" type="text/javascript"></script>
    <script src="../js/supervision.js" charset="utf-8" type="text/javascript"></script>
    <script src="../js/common.js" type="text/javascript" charset="utf-8"></script>
</html>