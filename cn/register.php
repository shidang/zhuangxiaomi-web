<?php
    include("../php/include.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>用户注册-装小蜜</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="../css/common.css" />
        <link rel="stylesheet" type="text/css" href="../css/login_register.css" />
    </head>
    <body>
        <div class="wrap">
           <?php
                include("head.php");
            ?>          
        </div>
        <!-- 登陆主要模块 -->
        <div class="wrap-jwy">
            <div class="login-jwy">
                <!-- 左边的图片 -->
                <div class="left-pic-jwy"></div>
                <!-- 右边列表 -->
                <div class="right-rform-jwy">
                    <form action="" method="">
                        <ul class="rform-jwy clearfix">
                            <li class="label-rupv-jwy">
                                <label for="username">手机号</label>
                                <input type="text" name="username" id="username" placeholder="username" />
                                <span class=""></span>
                            </li>
                            <li class="label-rupv-jwy">
                                <label for="password">输入密码</label>
                                <input type="password" name="password" id="password" placeholder="6~20位英文数字组成" />
                                <span class=""></span>
                            </li>
                            <li class="label-rupv-jwy">
                                <label for="password2">确认密码</label>
                                <input type="password" id="password2" placeholder="password" />
                                <span class=""></span>
                            </li>
                            <!-- 激活码 -->
                            <li class="activation-code-jwy">
                                <label for="activation-jwy">激活码</label>
                                <input type="text" id="activation-jwy" placeholder="verification" />
                                <input type="button" value="获取激活码"/>
                                <span class=""></span>
                            </li>
                        </ul>
                            <!-- 勾选服务协议 -->
                            <div class="userprotocol-jwy">
                                <input type="checkbox" id="userprotocol" />
                                <label for="userprotocol"> 我已阅读并同意遵守《装小蜜用户服务协议》</label>
                            </div>
                            <div class="sure-jwy clearfix">
                                <a href="login.php">已有账号 直接登陆</a>
                                <input type="submit" value="确定" id="submitjwy" data-reg="1"/> 
                            </div> 
                    </form>
                    <div class="tit-jwy">使用以下帐号直接登录</div>
                    <div class="con-bottom-jwy">
                        <a class="sina-jwy" href="" title=""></a>
                        <!-- <a class="qq-jwy" href="" title=""></a> -->
                        <a class="qq-jwy" target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                        <a class="weixin-jwy" href="" title=""></a>
                        <a class="dou-jwy" href="" title=""></a>
                    </div>
                </div>
            </div>          
        </div>
        <?php
            include("foot.html");
        ?>
    </body>
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript" src="../js/login_register.js"></script>
    <script type="text/javascript">
        regJwy("#username", reg.username);
        regJwy("#password", reg.password);
        balJwy("#password2", "#password");
        //后台方面
        //还需要一个cond函数
        var checkClick = true;
        $("#submitjwy").on("click", function(e){
            // 阻止表单提交的默认事件
            e.preventDefault();
            // 阻止多次点击产生的多次提交
            if(checkClick) {
                checkClick = false;
                var arr = [0, 0, 0];
                arr[0] = regJwy("#username", reg.username);
                arr[1] = regJwy("#password", reg.password);
                arr[2] = balJwy("#password2", "#password");
                var sum = 0;
                for(var i = 0; i < arr.length; i++){
                    sum += arr[i];
                }
                if(sum == 3){
                    $.post("../php/register_ajax.php", $("form").serializeArray(), function(response){
                        if(!response.state){
                           alert("用户名已存在");
                            checkClick = true;
                        } else {
                            console.log("a");
                            window.location.href = "login.php";
                        }
                    })
                } else {
                    checkClick = true;
                }
            }
        })
    </script>
</html>