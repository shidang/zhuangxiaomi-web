<?php 
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>用户登录-装小蜜</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="../css/common.css" />
        <link rel="stylesheet" type="text/css" href="../css/login_register.css" />
    </head>
    <body>
        <div class="wrap">
            <?php
                include("head.php");
            ?>          
        </div>
        <!-- 登陆主要模块 -->
        <div class="wrap-jwy">
            <div class="login-jwy">
                <!-- 左边的图片 -->
                <div class="left-pic-jwy"></div>
                <!-- 右边列表 -->
                <div class="right-form-jwy">
                    <form action="" method="get">
                        <ul class="form-jwy clearfix">
                            <li class="label-upv-jwy li-two-jwy">
                                <label for="username">手机号</label>
                                <input type="text" id="username" name="username" placeholder="Phone"/>
                            </li>
                            <li class="label-upv-jwy li-two-jwy">
                                <label for="password">密码</label>
                                <input type="password" id="password" name="password" placeholder="password" />
                            </li>
                            <!-- 验证码 -->
                            <li class="label-upv-jwy verification-code-jwy li-three-jwy">
                                <label for="verification-jwy">验证码</label>
                                <input type="text" id="verification-jwy" name="verification-jwy" placeholder="verification" />
                                <img src="" alt="验证码" title=""/>
                            </li>
                            <li class="li-four-jwy">
                                <input type="checkbox" name="" value="" id="remember-jwy">
                                <label for="remember-jwy">记住密码</label>
                                <a href="forgot-password1.php">忘记密码？</a>
                            </li>
                            <li class="btn-jwy">
                                <a href="register.php" title="">注册</a>
                                <label for="submit-jwy">登录</label>
                                <input type="submit" value="登陆" id="submit-jwy" />
                                <div class="tip"></div>
                            </li>
                        </ul>
                    </form>
                    <div class="tit-jwy">使用以下帐号直接登录</div>
                    <div class="con-bottom-jwy">
                        <a class="sina-jwy" href="" title=""></a>
                        <!-- <a class="qq-jwy" href="" title=""></a> -->
                        <a class="qq-jwy" target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                        <a class="weixin-jwy" href="" title=""></a>
                        <a class="dou-jwy" href="" title=""></a>
                    </div>
                </div>
            </div>           
        </div>
        <?php
            include("foot.html");
        ?>
    </body>
    <script type="text/javascript"  src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript">
        var cond = [0, 0];
        // 事件委托 检测e.target的id属性
        // 失焦检测
        $('#username').on('blur', function () {
            cond[0] = 0;
            var user = $(this).val();
            // 去除空格，
            user = user.replace(/\s+/g,"");
            // 将去除空格的名字重新赋值给文本框
            $(this).val(user);
            if (user != "") {
                cond[0] = 1;
            } else {
               $("#username").val("请输入用户名")
            }
        });
        $('#password').on('blur', function(){
            cond[1] = 0;
            var pass = $(this).val();
            pass = pass.replace(/\s+/g,"");
            $(this).val(pass);
            if (pass != "") {
                cond[1] = 1;
            } else {
                $("#password").val("请输入用密码");
            }
        })
        var checkClick = true;
        $("#submit-jwy").on("click", function(e){
            // 阻止表单提交的默认事件
            e.preventDefault();
            // 阻止多次点击产生的多次提交
            if(checkClick) {
                checkClick = false;
                $('#username').blur();
                $('#password').blur();
                var sum = 0;
                for(var i = 0; i < cond.length; i++){
                    sum += cond[i];
                }
                if(sum == 2){
                    $.post("../php/login_ajax.php", $("form").serializeArray(), function(response){
                        if(!response.state){
                            $(".tip").html("请输入用户名或密码")
                            checkClick = true;
                        } else {
                        window.location.href = "index.php";

                        }
                    })
                } else {
                    $(".tip").html("请输入用户名或密码")
                }
            }
        })
    </script>
</html>