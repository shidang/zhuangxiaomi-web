<?php 
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>装小蜜-蜜材联盟</title>
    <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
    <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
</head>
<body>
    <div class="wrap">
        <?php
            include("head.php");
        ?>
    </div>
    <div class="banner-picshow-wxh">
        <div class="activity-lll">
            <div id="carousel">
                <img src="../images/demo_images/12-1.jpg" alt="lady-bug">
                <img src="../images/demo_images/12-2.jpg" alt="caterpillar">
                <img src="../images/demo_images/12-3.jpg" alt="cactus">
                <img src="../images/demo_images/12-4.jpg" alt="snail">
                <img src="../images/demo_images/12-5.jpg" alt="penguin">
                <img src="../images/demo_images/12-6.jpg" alt="giraffe">
                <img src="../images/demo_images/12-7.jpg" alt="flower">
                <img src="../images/demo_images/12-8.jpg" alt="flower">
                <img src="../images/demo_images/12-9.jpg" alt="flower">
            </div>
        </div>
    </div>
    <div class="main-cql clearfix">
        <div class="main-left-cql">
            <h2></h2>
            <ul class="clearfix">
                <li>
                    <a href="http://sz.house.sina.com.cn/sznews/2005-08-29/1640242.html" title="活动图片" class="act-pic">
                        <img src="../images/demo_images/unite18.jpg" alt="" title="">   
                    </a>
                    <div class="unite-cql">
                        <h3>
                            <a href="http://sz.house.sina.com.cn/sznews/2005-08-29/1640242.html" title="">装小蜜主材团购第二期“需求投票结果”</a>
                        </h3>
                        <p>小蜜于近期在“蜜糖Club群”“主材咨询报备群”“装修主材团购club群”收集了大家的主材需求,为大家争取足够的利益，就算是小蜜贴钱也要帮大家把价格砍到底线。</p>
                        <div class="timer-cql">
                            <em>征集结束时间：征集结束时间：2015年9月26日</em>
                            <span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="http://www.ifeelings.com.cn/" title="活动图片" class="act-pic">
                        <img src="../images/demo_images/unite19.jpg" alt="" title="">   
                    </a>
                    <div class="unite-cql">
                        <h3>
                            <a href="http://www.ifeelings.com.cn/" title="">装小蜜主材团购活动“芬琳漆”</a>
                        </h3>
                        <p>小蜜于近期在“蜜糖Club群”“主材咨询报备群”“装修主材团购club群”收集了大家的主材需求,为大家争取足够的利益，就算是小蜜贴钱也要帮大家把价格砍到底线。</p>
                        <div class="timer-cql">
                            <em>征集结束时间：2015年10月1日-10月6日</em>
                            <span></span>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="http://www.fjangel.com/" title="活动图片" class="act-pic">
                        <img src="../images/demo_images/unite15.jpg" alt="" title="">   
                    </a>
                    <div class="unite-cql">
                        <h3>
                            <a href="http://www.fjangel.com/" title="">装小蜜主材团购活动第一期“美国3M净水器”</a>
                        </h3>
                        <p>为了真正有效的来帮助大家拿到同期市场零售价的最低价，为大家争取足够的利益，大家可在指定的时间内说出自己想要的，品牌、品类、型号、推荐理由发送到群内（装.李静）处进行报备。</p>
                        <div class="timer-cql">
                            <em>征集开始时间：2015-9-7</em></br>
                            <span>活动日期：2015-9-18</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="main-right-cql">
            <div class="inline-fuli-cql">
            </div> 
            <p>累计消费3000元以上业主，且资金通过装小蜜平台，享有100~1000元红包一个</p>
             <div class="inline-caigou-cql">
            </div> 
            <img src="../images/caigou2.jpg" alt="" title="采购流程">
        </div>
        <div class="icon-cql">
            <img src="../images/tubiao.jpg" alt="" title="">
        </div>
    </div>
    <div class="activity-wxh">
        <div class="shanjia-cql"></div>
        <div class="logolist-cql">
            <ul>
                <li>
                    <a href="http://www.marcopolo.com.cn/" title="马可波罗瓷砖">
                        <img src="../images/demo_images/img/1.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.oceano.com.cn/" title="欧神诺陶瓷">
                        <img src="../images/demo_images/img/2.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.meitaotc.com/" title="美陶瓷砖">
                        <img src="../images/demo_images/img/3.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.gdwm.cn/" title="唯美陶瓷">
                        <img src="../images/demo_images/img/4.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.cg1993.com/" title="冠珠陶瓷·卫浴">
                        <img src="../images/demo_images/img/5.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.garcia.net.cn/" title="加西亚瓷砖">
                        <img src="../images/demo_images/img/6.png" />
                    </a>
                    </li>
                <li>
                    <a href="http://www.dongpeng.net/" title="东鹏陶瓷">
                        <img src="../images/demo_images/img/7.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.nabel.cc/" title="诺贝尔瓷砖">
                        <img src="../images/demo_images/img/8.png" />
                    </a>
                </li>
                <li>
                    <a href="" title="Cerim">
                        <img src="../images/demo_images/img/9.png" />
                    </a>
                </li>
                <li>
                    <a href="http://newnanyue.com/" title="新南悦磁砖">
                        <img src="../images/demo_images/img/10.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.tidiy.com.cn/" title="特地瓷砖">
                        <img src="../images/demo_images/img/11.png" />
                    </a>
                </li>
                <li>
                    <a href="" title="简一陶瓷">
                        <img src="../images/demo_images/img/12.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.romaceramic.com.cn/" title="罗马瓷砖">
                        <img src="../images/demo_images/img/13.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.sinocrane.com/" title="华鹤家具">
                        <img src="../images/demo_images/img/17.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.holike.com/" title="好莱客">
                        <img src="../images/demo_images/img/19.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.zgblmt.com/" title="别丽美特壁纸">
                        <img src="../images/demo_images/img/22.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.yjlt.com/" title="艺极楼梯">
                        <img src="../images/demo_images/img/23.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.auspoll.com.cn/" title="欧斯宝吊顶定制家">
                        <img src="../images/demo_images/img/24.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.51efc.com/" title="品屋">
                        <img src="../images/demo_images/img/25.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.rasch.com.cn/" title="德国朗饰壁纸">
                        <img src="../images/demo_images/img/26.png" />
                    </a>
                </li>
                <li>
                    <a href="http://chinayoubang.com/" title="友邦集成吊顶">
                        <img src="../images/demo_images/img/27.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.lsgzn.com/" title="兰舍硅藻泥">
                        <img src="../images/demo_images/img/28.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.ttmcn.com/" title="一品和">
                        <img src="../images/demo_images/img/29.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.tcl.com/" title="TCL">
                        <img src="../images/demo_images/img/30.png" />
                    </a>
                </li>
                <li>
                    <a href="http://www.philips.com.cn/" title="PHILIPS">
                        <img src="../images/demo_images/img/31.png" />
                    </a>
                </li>
            </ul>
        </div>           
    </div>
    <div class="returntop-cql">
        <ul>
            <li class="code-cql">
                <img src="../images/weixin.png" alt="" title="" class="lefthide-cql">
            </li>
            <li class="qq-cql">
                <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                <div class="qqspan">
                    <span>周一至周五</span>
                    <span>9:00-21:00</span>
                </div>
            </li>
            <li class="return-cql" id="returnTop">
            </li>
        </ul>
    </div>
    <?php
        include("foot.html");
    ?>    
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript" src="../js/supervision.js"></script>
    <script src="../js/jquery.carouFredSel-6.0.4-packed.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#carousel').carouFredSel({
                items: 3,
                scroll: {
                    duration: 1400,
                    timeoutDuration: 2500,
                    onBefore: function( data ) {
                        data.items.old.each(function( i ) {
                            $(this).delay(i*150).animate({
                                marginTop: 250,
                                marginBottom: -250
                            }, 500);
                        });
                        data.items.visible.css({
                            marginTop: -250,
                            marginBottom: 250
                        });
                        data.items.visible.each(function( i ) {
                            $(this).delay((3+i)*150).animate({
                                marginTop: 0,
                                marginBottom: 0
                            }, 500);
                        });
                    }
                }
            });
        });
    </script>
</body>
</html>