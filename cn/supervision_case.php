<?php
    session_start();
    include("../php/include.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>监理案例</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" href="../css/common.css">
        <link rel="stylesheet" href="../css/page.css">
    </head>
    <body>
    <div class="wrap">
        <?php
            include("head.php");
        ?>
    </div> 
    <div class="case">
        <div class="main">
            <ul>
                <li id="area-class" class="area-class">
                    <span>区域划分</span>
                    <a href="javascript:" class="select" title="区域划分" alt="">全部</a>
                    <a href="javascript:" title="" alt="">朝阳区</a>
                    <a href="javascript:" title="" alt="">门头沟区</a>
                    <a href="javascript:" title="" alt="">平谷区</a>
                    <a href="javascript:" title="" alt="">石景山区</a>
                    <a href="javascript:" title="" alt="">房山区</a>
                    <a href="javascript:" title="" alt="">延庆县</a>
                    <a href="javascript:" title="" alt="">宣武区</a>
                    <a href="javascript:" title="" alt="">崇文区</a>
                    <a href="javascript:" title="" alt="">通州区</a>
                    <a href="javascript:" title="" alt="">顺义区</a>
                    <a href="javascript:" title="" alt="">怀柔区</a>
                    <a href="javascript:" title="" alt="">大兴区</a>
                    <a href="javascript:" title="" alt="">密云县</a>
                    <div id="expend_btn" class="move_drop"></div>
                    <div class="morearea">
                        <a href="javascript:" title="" alt="">宣武区</a>
                        <a href="javascript:" title="" alt="">崇文区</a>
                        <a href="javascript:" title="" alt="">通州区</a>
                    </div>
                </li>
                <li class="status">
                    <span>工地阶段</span>
                    <a href="javascript:;" class="select" title="" alt="">全部</a>
                    <a href="javascript:;" title="" alt="">施工中</a>
                    <a href="javascript:;" title="" alt="">已完工</a>
                </li>
                <li class="house-type">
                    <span>户型结构</span>
                    <a href="javascript:;" class="select" title="" alt="">全部</a>
                    <a href="javascript:;" title="" alt="">平房</a>
                    <a href="javascript:;" title="" alt="">一居</a>
                    <a href="javascript:;" title="" alt="">二居</a>
                    <a href="javascript:;" title="" alt="">三居</a>
                    <a href="javascript:;" title="" alt="">跃层</a>
                    <a href="javascript:;" title="" alt="">复式</a>
                    <a href="javascript:;" title="" alt="">联排</a>
                    <a href="javascript:;" title="" alt="">别墅</a>
                </li>
                <li class="budget">
                    <span>装修预算</span>
                    <a href="javascript:;" class="select" title="" alt="">全部</a>
                    <a href="javascript:;" title="" alt="">5万以下</a>
                    <a href="javascript:;" title="" alt="">5-10万</a>
                    <a href="javascript:;" title="" alt="">10万-15万</a>
                    <a href="javascript:;" title="" alt="">15-20万</a>
                    <a href="javascript:;" title="" alt="">20万以上</a>
                </li>
                <li class="house-area">
                    <span>房屋面积</span>
                    <a href="javascript:;" class="select" title="" alt="">全部</a>
                    <a href="javascript:;" title="" alt="">0-60㎡</a>
                    <a href="javascript:;" title="" alt="">60-100㎡</a>
                    <a href="javascript:;" title="" alt="">100-150㎡</a>
                    <a href="javascript:;" title="" alt="">150-300㎡</a>
                    <a href="javascript:;" title="" alt="">300㎡以上</a>
                </li>
            </ul>
        </div>
        <div id="ui-class" class="ui-class">展开分类</div>
        <div class="building">
            <div class="info-class clearfix">
                <h2>共检索出135块工地</h2>
                <span class="com">评论量</span>
                <span>浏览量</span>
                <span class="show">发布时间</span>
            </div>
            <div class="user-info clearfix">
            <?php 
                $sql = mysql_query("SELECT * FROM supervision_case");
                while($row = mysql_fetch_assoc($sql)) {
            ?>
              <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php?aid=<?php echo $row['id'];?>" title="案例" alt="照片">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2><?php echo $row["houseName"]; ?></h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num"><?php echo $row    ["step"]; ?></span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                 <?php } ?>
               <!--  <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
               -->
               <!--  <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl> -->
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <dl class="user-one">
                    <dt class="clearfix">
                        <a href="supervision_case_details.php" title="" alt="">
                            <img src="../images/demo_images/info1.png" alt="1">
                        </a>
                    </dt>
                    <dd class="photo">
                        <img src="../images/demo_images/userImg2.png" alt="user">
                        <h3>林禹凡</h3>
                        <span>监理</span>
                        <p>监理工程师 从业12年</p>
                        <span class="over">施工中</span>
                    </dd>
                    <dd class="concrete">
                        <h2>背景麒麟瑞景别墅</h2>
                        <ul>
                            <li>面积：<em>135m²</em></li>
                            <li>决算：<em>23s万元（预算36万元）</em></li>
                            <li>实际工期：<em>32天（预计工期 45天）</em></li>
                            <li>业主：<em>李女士</em></li>
                            <li>工长：<em>张元培</em></li>
                            <li>完成进度：<span class="over-num">65%</span><span class="over"></span></li>
                        </ul>
                    </dd>
                </dl>
                <q class="more">更多案例</q>
            </div>
            <div class="loading"></div>
        </div>
    </div>
    <div class="returntop-cql">
        <ul>
            <li class="code-cql">
                <img src="../images/weixin.png" alt="" title="" class="lefthide-cql">
            </li>
            <li class="qq-cql">
                <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=846758148&site=qq&menu=yes" title="装小蜜QQ"></a>
                <div class="qqspan">
                    <span>周一至周五</span>
                    <span>9:00-21:00</span>
                </div>
            </li>
            <li class="return-cql" id="returnTop">
            </li>
        </ul>
    </div>
    <?php
        include("foot.html");
    ?>        
    </body>
    <script src="../js/jquery-1.11.3.min.js" charset="utf-8" type="text/javascript"></script>
    <script src="../js/common.js" charset="utf-8" type="text/javascript"></script>
    <script src="../js/supervision.js" charset="utf-8" type="text/javascript"></script>
    <script type="text/javascript">
        // JSON数据交互，实现懒加载部分
        var strJwy = "";
        var numCountJwy = 0; 
        var winJwy = $(window).height();
        var docSd = $("body").height();              
        winJwy = (winJwy / 3 * 2);
        $(window).scroll(function(){
            wyScroll = $(this).scrollTop();
            docSd = $("body").height();
           if(docSd >= 3000){
                $(".more").css({
                    "display": "none"
                })
           }else {
                if(wyScroll >= winJwy){
                    $.ajax({
                        url : "../php/case.json",
                        type : "get",
                        success: function(data){
                            for(var i = 0; i < 4; i ++){
                                 // strJwy +="<h2>" + data[i] + "</h2>";
                                strJwy +=" <dl class='user-one'><dt class='clearfix'><a href='supervision_case_details.php' title='' alt=''><img src='../images/demo_images/info1.png' alt='1'></a></dt><dd class='photo'><img src='../images/demo_images/userImg2.png' alt='user'><h3>林禹凡</h3><span>监理</span><p>监理工程师 从业12年</p><span class='over'>施工中</span></dd><dd class='concrete'><h2>" + data[i] + "</h2><ul><li>面积：<em>135m²</em></li><li>决算：<em>23万元（预算36万元）</em></li><li>实际工期：<em>32天（预计工期 45天）</em></li><li>业主：<em>李女士</em></li><li>工长：<em>张元培</em></li><li>完成进度：<span class='over-num'>65%</span><span class='over'></span></li></ul></dd></dl>"
                             } 
                            $(".more").before(
                                    strJwy
                            )}
                    })                            
                }
           }                  
        })
    </script>
</html>