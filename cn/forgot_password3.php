<!DOCTYPE html>
<html lang="en">
    <head>
         <meta charset="utf-8">
        <title>忘记密码</title>
        <link rel="icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" /> 
        <link rel="shortcut icon" href="../images/zhuangxiaomi.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="../css/common.css" />
        <link rel="stylesheet" type="text/css" href="../css/login_register.css" />
    </head>
    <body class="wrap-jwy">
        <div class="wrap">
            <?php
                include("head.head");
            ?>          
        </div>
        <div class="forgot-box-jwy">
            <div class="clearfix">
                <span class="forgot-step-jwy">1.输入手机号码  ></span>
                <span>2.输入验证码  ></span>
                <span>3.重置密码</span>
            </div>
            <div class="forgot-con-jwy">
                <form action="" method="">
                <!-- 新密码 -->
                    <div class="newpass-jwy clearfix">
                        <label for="newbgpaJwy"></label>
                        <input type="text" name="" id="newbgpaJwy" placeholder="新秘密" />
                    </div>
                    <!-- 确认密码 -->
                    <div class="surepass-jwy cleafix">
                        <label for="surebgpassJwy"></label>
                        <input type="text" name="" id="surebgpassJwy" placeholder="确认密码" /> 
                        <img src="" alt="" title=""/>
                    </div>
                    <!-- 确认 -->
                    <div class="forgot-sub-jwy">
                        <input type="submit" value="确认" />
                    </div>
                </form>
            </div>
        </div>
        <?php
            include("foot.html");
        ?>
    </body>
    <script src="../js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript" src="../js/login_register.js"></script>
    <script type="text/javascript">
        // 聚焦后样式变化
        telBgJwy(".newpass-jwy", verBgFous, verBgBlur);
        telBgJwy(".surepass-jwy", verBgFous, verBgBlur);
        balJwy("#surebgpassJwy", "#newbgpaJwy");
    </script>
</html>