/* 
* @Author: cql
* @Date:   2015-10-20 09:56:44
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-24 22:49:02
*/

$(document).ready(function(){
         // 广告淡入淡出
        var liindex = 0;
        var hereindex = 0;
        var timer_cql = null;
        function timer() {
            timer_cql = setInterval(function() {
                fade(0);
            },5000);
        } 
        timer();
        function fade() {
            $(".banner-cql li").eq(liindex).fadeIn(1500).siblings().fadeOut(500);
            liindex++;
            $(".footer-btn-cql span").eq(hereindex).addClass('here-cql').siblings().removeClass('here-cql');
            hereindex++;
            if(liindex == $(".banner-cql li").length) {
                liindex = 0;
            }
            if(hereindex == $(".footer-btn-cql span").length) {
                hereindex = 0;
            }
        }fade(0);   
       // 广告中的底部按钮
        $(".footer-btn-cql span").click(function() {
            if(timer_cql){
                clearInterval(timer_cql);
            }
            liindex = $(this).index();
            hereindex = liindex;
            $(".banner-cql li").eq($(this).index()).fadeIn(100).delay(1000).siblings().fadeOut(100);
            $(".footer-btn-cql span").eq(hereindex).addClass('here-cql').siblings().removeClass('here-cql');
            fade($(this).index());
            timer();
        }) 
        
     // 监理数据支持模块鼠标hover效果
     // 功能描述：鼠标移入，li,上滑 
        $(".supportlist-cql li").hover(function() {
            $(this).children().find("div").stop(true, true).animate({
                "height" : "355px",
                "top" : "0"
            },300)
            $(this).children().find(".sliderhide h4").css({
                "padding-top" : "116px"
            })
        },function() {
            $(this).children().find("div").stop(true, true).animate({
                "height" : "90px",
                "top" : "265px"
            },300)
            $(this).children().find(".sliderhide h4").css({
                "padding-top" : "0px"
            })
        });
        // 四栏模块
        $(".four-title-main span").mouseenter(function() {
            $(this).addClass('four-sel-cql').siblings().removeClass('four-sel-cql').parent().parent().parent().siblings().children().eq($(this).index()).addClass('content-show-cql').siblings().removeClass('content-show-cql');
        });
        // 权威报道
        var flag = false;
        var outerC = $("#outerC");
        function cql() {
            var directionright = 277;
            var directionleft = -277;
            $(".btn-right-cql").click(function() {
                outerC.animate({
                    "scrollLeft" : directionright
                }, 500)
            });
            $(".btn-left-cql").click(function() {
                outerC.animate({
                    "scrollLeft" : directionleft
                })       
            });
         }cql()

         // 手风琴制作
        // 每次更改浏览器大小，重新获取宽度
        $(window).resize(function(){
            accordionshow();
        });
        accordionshow();
        // 实现效果的函数封装
        function accordionshow() {
            var win_width = $(window).width();//获取浏览器宽度
            var _each_picture = Math.floor(win_width / 6);//计算每个图片的宽度
            var smallpic = Math.floor(_each_picture - (_each_picture * 0.1));//触发效果后的非当前图片变小
            var bigpic = Math.floor(_each_picture + (_each_picture * 0.5));//触发效果后的当前图片变大
            var flage = 0;//实现初始化第一个图片的效果；

            // 上排照片效果的实现函数
            // _gallery表示获取的标签；
            // _gfirst表示一组标签中的第一个；
            function accordionshow1(){
                var _gallery = $('.accordion .accordion-show1 li');
                var _gfirst = $('.accordion .accordion-show1 li').first();
                var _info1 = $(".accordion .accordion-show1 .infoma-wxh");
                _info1.css({
                    width: _each_picture
                })
                _gfirst.css({
                    width: _each_picture
                }).siblings().css({
                    width: _each_picture
                });
                accordion(_gallery, _gfirst, _info1);//传递参数，实现效果
            }accordionshow1();

            // 下排照片的效果的实现函数
            function accordionshow2(){
                var _gallery = $('.accordion .accordion-show2 li');
                var _gfirst = $('.accordion .accordion-show2 li').first();
                // 获取信息部分
                var _info2 = $(".accordion .accordion-show2 .infoma-wxh");
                _info2.css({
                    width: _each_picture
                })
                _gfirst.css({
                    width: _each_picture
                }).siblings().css({
                    width: _each_picture
                });
                accordion(_gallery, _gfirst, _info2);//传递参数，实现效果
            }accordionshow2();

            // 所有照片实现的效果
            // labelname标签名；labelnamefirst第一个标签，labelinfo表示信息
            function accordion(labelname, labelnamefirst, labelinfo){
                labelname.hover(function(){
                    // alert(1);
                    if (flage == 0) {
                        flage = 1;
                        labelnamefirst.stop().animate({
                            width: _each_picture
                        }, 300).siblings().stop().animate({
                            width: _each_picture
                        }, 300);
                    }
                    labelinfo.eq($(this).index()).stop(true, true).animate({"top" : "168px"}, 300).css({"width" : "100%"});

                    $(this).siblings().stop().animate({
                        width: smallpic 
                    }, 300);
                    $(this).stop().animate({
                        width: bigpic 
                    }, 300);
                    }, function(){
                        labelinfo.eq($(this).index()).stop(true, true).animate({"top" : "250px"}, 300);

                        $(this).stop().animate({
                            width: _each_picture
                        }, 300);
                        $(this).siblings().stop().animate({
                            width: _each_picture
                        }, 300);
                    }
                );  
            }           
        }
});