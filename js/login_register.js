/* 
* @Author: Jwy
* @Date:   2015-10-18 20:41:15
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-25 09:42:37
* 登录注册
*/
//reg中的方法为正则表达式，username对应的是电话号码，password对应的是密码
var reg = {
            username: /^1[3|4|5|8][0-9]\d{4,8}$/,
            password: /^[^\u4e00-\u9fa5]{6,20}$/g
        };
//第一个参数代表ID名字，第二参数是需要调用的正则表达式。书写方式为reg.属性名字如（regJwy("#password", reg.password);）
var arrJwy = 0;
function regJwy(idName, regName) {
    $(idName).on("focus", function(){
        $(idName).css({
            "color" : "#000"
        })
    })
    $(idName).on('blur', function(){
        var regA = regName;
        var user = $(this).val();
        user = user.replace(/\s+/g,"");
        $(this).val(user);
        if(regA.test(user)) {
            $(idName).css({
                'color' : "#000"
            }).siblings("span").addClass("rightspan");
            arrJwy = 1;
        } else {
            $(idName).css({
                'color' : "#fe0000"
            }).siblings("span").removeClass("rightspan")
            arrJwy = 0;
        }
    })
    return arrJwy;
}
//比较两个Inout是否相等的函数，numA为pass2（比较参数）numB为pass（被比较参数）两个形参的内容为id或类名如（balJwy("#password2", "#password");）
function balJwy(numA, numB) {
     $(numA).on('blur', function(){
            var pass = $(numA).val();
            var pass2 = $(numB).val();
            if(pass == pass2) {
                $(numA).css({
                    'color' : "#000"
                }).siblings("span").addClass("rightspan")
                arrJwy = 1;
            } else {
                 $(numA).css({
                    'color' : "#fe0000"
                }).siblings("span").removeClass("rightspan")
                 arrJwy = 0;
            }
            
        })
     return arrJwy;
}
/* 
* @Author: Jwy
* @Date:   2015-10-18 20:41:15
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-18 21:22:39
* 忘记密码
*/
var telBgFous = "#fff url(../images/zxm_bg.png) 503px -319px";
var telbgBlur = "#fff url(../images/zxm_bg.png) 479px -319px";
var verBgFous = "#fff url(../images/zxm_bg.png) 551px -317px";
var verBgBlur = "#fff url(../images/zxm_bg.png) 527px -317px";



// 修改密码找回页面的失焦聚焦状态函数
function telBgJwy(claName, bgstyA, bgstyB){
        var claIn = claName + " input[type=text]";
        var claLa = claName + " label";
        $(claIn).focus(function() {
            $(claLa).css({
              "background": bgstyA  
            })
      })
      $(claIn).blur(function() {
            $(claLa).css({
              "background": bgstyB  
            })
      })
    }  

/* 
* @Author: Jwy
* @Date:   2015-10-18 20:41:15
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-20 13:28:14
* 用户中心内容
*/
    // 这里是新的部分
    //安全信息部分点击。下滑出现内容;
    //JWY
    //anJwy为判断是展开还是收缩，0收缩，1展开Idame为点击的标签，changeName为变化的标签，startlen缩短的距离，endlen开始的距离,startMarendMar是margintop的开始，endMar是margintop的结束
    function spread(IdName, changeName, startlen, endlen, startMar, endMar) {
        var anJwy = 1;
        $(IdName).click(function(){
            if(anJwy == 0) {
                $(changeName).stop().animate({
                    height : startlen,
                    marginTop : startMar
                }, 500)
                anJwy = 1;
            } else {
                $(changeName).stop().animate({
                    height : endlen,
                    marginTop : endMar
                }, 500)
                anJwy = 0;
            }
        })
    }
    spread("#loginpassjwy", "#loginpasscon", 0, 203, 4, 18);
    spread("#loginmailjwy", "#loginmailcon", 0, 158, 4, 18);