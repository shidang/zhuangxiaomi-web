/* 
* @Author: wxh
* @Date:   2015-10-20 10:15:45
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-24 14:31:03
* 头部导航
*/
$(document).ready(function(){
    // 变量：timer定义为计时器；step:一次走多少px；indent表示哪个标签，obj表示需要的哪个属性对象；prop表示对应的属性值；
        // 函数：navMove是控制函数；aMove是改变线条位置函数；
        // bool的值从登录页面中传递而来，true,则回复初始位置并隐藏，false则显示并移动；此时初始为false
    var navLists = document.getElementById("nav-list").getElementsByTagName("a");
    var lineJwy = document.getElementById('line-jwy');
    var timer = null;
    for(var i = 0; i < navLists.length; i++){
        navLists[i].index = i;
        navLists[i].onmouseenter = function(){
            var len = this.index * 111;
            // console.log(len);
            navMove(lineJwy, {"left": len}, 10)
        }
    }
    function navMove(indent, obj, step) {
        if(indent.timer){
            clearInterval(indent.timer);
        }
        indent.timer = setInterval(lineMove, 12);
        function lineMove() {
            bool = false;
            for(var prop in obj){
                // 获取left值为初始位置
                var startVal = parseInt(getClass(indent, prop));
            }
            // 获取相差距离：目标值-初始值
            var dis = Math.abs(obj[prop] - startVal);
            // 计算速度
            var speed = Math.ceil(dis / step);
            
            if(speed == 0){
                clearInterval(indent.timer);
            }
            if(startVal < obj[prop]){
                startVal += speed;
            } else {
                startVal -= speed;
            }
            if(bool == true){
                indent.style.left = "0px";
                indent.style.display = "none";
            } else {
                indent.style.display = "block";
                indent.style[prop] = startVal + "px";
            }
        }
    }
    // 获取元素的属性值
    function getClass(ele, name){
        if(ele.currentStyle){
            return ele.currentStyle[name];
        } else {
            return window.getComputedStyle(ele, null)[name];
        }
    }
    /* 
    * @Author: cql
    * @Date:   2015-10-19 15:10:46
    * @Last Modified by:   anchen
    * @Last Modified time: 2015-10-19 15:25:28
    * 返回顶部
    */
    $(window).scroll(function() {
        var winTop = $("body").scrollTop() || $("html, body").scrollTop();
        if (winTop >= 650) {
        $(".returntop-cql").fadeIn();
            $(".return-cql").click(function() {
                if($("body").scrollTop()){
                    $("body").stop(true).animate({
                        "scrollTop": "0"
                    }, 1000);
                } else {
                    $("html, body").stop(true).animate({
                        "scrollTop": "0"
                    }, 1000);
                } 
            })
        } else {
            $(".returntop-cql").fadeOut();
        }
    });
});

















