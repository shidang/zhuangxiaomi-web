/* 
* @Author: LLL
* @Date:   2015-10-17 00:31:09
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-24 23:10:29
监理服务
*/
$(function() {
    var hover = true;
    $(".service .project").hover(function() {
        if(hover) {
            $(this).stop(true).animate({
                marginTop : "-90px",
                paddingBottom : "410px"
            }, 300);
            $(".service .title").slideUp(300);
            hover = false;  
        }
    }, function() {
        $(this).stop(true).animate({
            marginTop : "0",
            paddingBottom : "60px"
        }, 300);
        $(".service .title").slideDown(300);
        $(".service .poj-con").children("div").fadeOut(100);
        $(".service .poj-con span").css({"display" : "none"});
        hover = true;
    });

    // 鼠标移入导航颜色变深，并显示下方的列表
    $(".service .marginbox div").each(function(index) {
        $(this).hover(function() {
            $(".service .poj-con span").css({
                "display" : "block",
                "left" : index * 186 + "px"
            })
            $(this).parent().siblings(".poj-con").children("div").eq($(this).index()).fadeIn(100).siblings("div").fadeOut(100);
        })
    });

    // 常见问题模块实现下拉效果
    $(".service .program-list div").each(function(index) {
        $(".service .program-list div").eq($(this).index()).click(function() {
            $(".service .program-list div").eq($(this).index()).children("p").slideDown(200);
            $(".service .program-list h3").eq($(this).index()).css({"color" : "#ffa650"}).parent().siblings().children("h3").css({"color" : "black"});
            $(".service .program-list span").eq($(this).index()).css({"background" : "url(../images/zxm_bg.png) -499px -327px no-repeat"}).parent().parent().siblings().children("h3").children("span").css({"background" : "url(../images/zxm_bg.png) -480px -327px no-repeat"});
            $(".service .program-list div").eq($(this).index()).siblings().children("p").slideUp(200);
        })
    });

/* 
* @Author: LLL
* @Date:   2015-10-17 00:31:09
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-20 16:12:31
监理团队
*/
    $(".svteam .people dt").click(function() {
        $(".svteam .wrap-pic").stop().animate({
            "top" : "0",
            "bottom" : "0"
        }, 1000, function() {
            $(".svteam .wrap-pic img").fadeIn(1000),
            $(".svteam .wrap-pic button").fadeIn(1000)
        });
    })
    $(".svteam button").click(function() {
        $(".svteam .wrap-pic img").fadeOut(1);
        $(".svteam .wrap-pic button").fadeOut(1);
        $(".svteam .wrap-pic").stop().animate({
            "top" : "50%",
            "bottom" : "50%"
        }, 1000);
    })
})

/* 
* @Author: lll
* @Date:   2015-10-18 11:23:45
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-20 10:42:00
* 监理案例页
*/
function clickup(){  
    // 点击展开与合上
    var bool = 0;
    $(".case .ui-class").click(function() {
        if(bool == 0) {
            $(".case .main ul").css({"display": "block"});
            $(".case .main ul").stop(true, true).animate({
                "height" : "262px"
            }, 400);
            $(".case .main").stop(true, true, true).animate({
                "height" : "262px"
            }, 400);
            $(".case .ui-class").stop(true, true, true).animate({
                "top" : "240px"
            }, 400);
            bool = 1; 
        } else if(bool == 1) {
            $(".case .main ul").slideUp(400);
            $(".case .main").stop(true, true, true).animate({
                "height" : "80px"
            }, 400);
            $(".case .ui-class").stop(true, true, true).animate({
                "top" : "58px"
            }, 400);
            // $(".case .main ul").css({"display": "none"});
            bool = 0; 
        }
        
    })

    // 加号展开
    var att = 0;
    $(".case .move_drop").click(function() {
        if(att == 0) {
            $(".case .morearea").css({
                "display" : "block"
            })
            att = 1;
            $(".case .move_drop").css({
                "background" : "url(../images/zxm_bg.png) -522px 6px no-repeat"
            })
        } else if(att == 1) {
            $(".case .morearea").css({
                "display" : "none"
            })
            att = 0;
            $(".case .move_drop").css({
                "background" : "url(../images/zxm_bg.png) -505px 6px no-repeat"
            })

        }
        
    })

    // 鼠标放上显示具体内容
    $(".case").on("mouseover", "dt", function() {
        var numll = $(this).parent().index() + 1;
        if(numll % 4 == 0) {
            $(this).siblings(".concrete").css({
                "left" : "-285px"
            })
        }
        $(this).siblings(".concrete").css({
            "display" : "block"
        })
    })
    $(".case").on("mouseout", "dl", function() {
        $(".case .user-one .concrete").css({
            "display" : "none"
         })
    });
}clickup();

/* 
* @Author: nyh
* @Date:   2015-10-17 00:31:09
* @Last Modified by:   anchen
* @Last Modified time: 2015-10-20 16:12:31
蜜材活动
*/
$(window).load(function(){
    var spotlight = {
    // 图片的透明度，可以修改。
    opacity : 0.3,
    // 图片宽高
    imgWidth : $(".logolist-cql ul li").find("img").width(), 
    imgHeight : $(".logolist-cql ul li").find("img").height() 
};

// 设置li宽高等于图片宽高
$(".logolist-cql ul li").css({"width" : spotlight.imgWidth, "height" : spotlight.imgHeight });
    // 移动到li的hover效果
    $(".logolist-cql ul li").hover(function(){
    // 添加类active，并更改图片透明度为1
        $(this).find("img").addClass("active").css({ "opacity" : 1});
    // 其他图片还原透明度
        $(this).siblings("li").find("img").css({"opacity" : spotlight.opacity}) ;
    // 鼠标离开效果
    }, function(){
    // 清除active类
        $(this).find("img").removeClass("active");
    });
// 鼠标离开ul效果
    $(".logolist-cql ul").bind("mouseleave",function(){
    // 图片opacity为1
    $(this).find("img").css("opacity", 1);
    });
});